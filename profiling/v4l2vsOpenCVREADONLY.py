import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


df = pd.read_csv("preparedData/v4l2_opencv_readonly.txt", sep=':')

framerate = []
sns.set(style="whitegrid")
plot = sns.barplot(y='framerate', x='size', hue='lib', data=df)
plot.set(ylim=(0 ,df['framerate'].max() + 4))
plot.set_title("Comparación de tasas de fotogramas entre softwares,\nEn Raspberry Pi usando la cámaranetway")
plot.legend(loc='lower left')
fig = plot.get_figure()

ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=11, color='gray', xytext=(0, 20),
                 textcoords='offset points')

fig.savefig("plots/v4l2vsocvreadonly.png")