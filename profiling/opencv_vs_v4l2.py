import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

sns.set(style="whitegrid")

df  = pd.read_csv("preparedData/v4l2VSopencv.txt", sep=':')
framerate = []
numRows = len(df.index)
for i in range(numRows):
    framerateAUX = df.at[i,'num_frames'] / df.at[i,'compute_time']
    framerate.append(framerateAUX)
df['framerate'] = framerate
plot = sns.barplot(y='framerate', x='libUsed', hue='operation', data=df)
plot.set(ylim=(0 ,df['framerate'].max() + 4))
plot.set_title("Webcam framerate comparison, using OpenCV and V4L2 on raspberry\ngauss filter width = 9, video extracted from webcam")
fig = plot.get_figure()
ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=11, color='gray', xytext=(0, 20),
                 textcoords='offset points')
fig.savefig("plots/v4l2vsOpencv.png")