import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


df = pd.read_csv("rawdata/resultsFinal.txt", sep=':')

framerate = []
numRows = len(df.index)
print(numRows)
for i in range(numRows):
    framerateAUX = df.at[i,'numframes'] / df.at[i,'tiempo']
    framerate.append(framerateAUX)
df['fotogramas/segundo'] = framerate
sns.set(style="whitegrid")
plot = sns.barplot(y='fotogramas/segundo', x='version', data=df)
plot.set(ylim=(0 ,df['fotogramas/segundo'].max() + 10))
plot.set_title("Comparativa warpPerspective en Raspberry Pi, \nvídeo obtenido de webcam")
fig = plot.get_figure()
ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=8, color='gray', xytext=(0, 20),
                 textcoords='offset points')
fig.savefig("plots/warpPerspective.png")
