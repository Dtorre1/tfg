import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

sns.set(style="whitegrid")

df = pd.read_csv("rawdata/resultsBuffer.txt", sep=':')

framerate_gr = []
numRows = len(df.index)
for i in range(numRows):
    framerateAUX = df.at[i,'Num_frames'] / df.at[i,'compute_time']
    framerate_gr.append(framerateAUX)
df['Fotogramas/segundo'] = framerate_gr
plot = sns.barplot(y='Fotogramas/segundo', x='buffer-size', hue='operation', data=df)
plot.set(ylim=(0 ,df['Fotogramas/segundo'].max() + 4))
plot.set_title("Comparación de tamaños de buffer en OpenMP tasks,\nAncho de función gaussiana = 5, vídeo obtenido de Webcam")
plot.legend(loc='lower left')
fig = plot.get_figure()
ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=7, color='gray', xytext=(0, 20),
                 textcoords='offset points')
fig.savefig("plots/bufferSize.png")