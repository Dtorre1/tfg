import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


df = pd.read_csv("preparedData/blockSize640PC.txt", sep=':')

framerate = []
numRows = len(df.index)
print(numRows)
for i in range(numRows):
    framerateAUX = df.at[i,'Num_frames'] / df.at[i,'compute_time']
    framerate.append(framerateAUX)
df['Fotogramas/segundo'] = framerate
sns.set(style="whitegrid")
plot = sns.barplot(y='Fotogramas/segundo', x='Block-size',hue='operation', data=df)
plot.set(ylim=(0 ,df['Fotogramas/segundo'].max() + 4))
plot.set_title("Tamaño de Work-group en nodo x86,\nAncho de función gaussiana = 5, vídeo obtenido de webcam")
plot.legend(loc='lower left')
fig = plot.get_figure()
ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=8, color='gray', xytext=(0, 20),
                 textcoords='offset points')
fig.savefig("plots/blocksizePC640.png")

