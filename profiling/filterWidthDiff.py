import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_csv("preparedData/filterWidthDiff.txt", sep=':')

framerate = []
numRows = len(df.index)
print(numRows)
for i in range(numRows):
    framerateAUX = df.at[i,'num_frames'] / df.at[i,'compute_time']
    framerate.append(framerateAUX)
df['Fotogramas/segundo'] = framerate
sns.set(style="whitegrid")
plot = sns.barplot(y='Fotogramas/segundo', x='version', data=df)
plot.set(ylim=(0 ,df['Fotogramas/segundo'].max() + 4))
plot.set_title("Comparación entre anchuras de función gaussiana, 9 y 5,\nVídeo extraído de Webcam, ejecutado en Raspberry Pi")
fig = plot.get_figure()
ax=plot
for p in ax.patches:
             ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                 ha='center', va='center', fontsize=11, color='gray', xytext=(0, 20),
                 textcoords='offset points')
fig.savefig("plots/filterWidth.png")

