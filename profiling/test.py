import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


df  = pd.read_csv("preparedData/test.txt", sep=':')
plot = sns.barplot(y='ValorY', x='ValorX', data=df)
fig = plot.get_figure()
fig.savefig("plots/test.png")

fig = plt.figure()