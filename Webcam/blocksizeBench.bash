#!/bin/bash
make OCL
echo $i >> resultsOCL.txt

for k in gauss grayscale;
do
for i in 32 64 128 256 512 1024 2048;
    do
        for j in {1..12};
        do
            cat params.txt | sudo ./opencl_webcam_OCL /dev/video0 10 20 640 480 $k 10 $i 5 >> resultsOCL.txt
            sleep 5
        done
    done
done
mv out.avi in.avi
echo "videofile"
for k in gauss grayscale;
do
for i in 32 64 128 256 512 1024 2048;
    do
        for j in {1..12};
        do
            cat params.txt | sudo ./opencl_webcam_OCL in.avi 10 20 640 480 $k 10 $i 5 >> resultsOCL.txt
            sleep 5
        done
    done
done