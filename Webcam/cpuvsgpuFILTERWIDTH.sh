#!/bin/bash
echo "CPU" >> filterwidth.txt
for i in 5 9 13;
do
    echo "filterWidth $i" >> filterwidth.txt
    for j in {1..12};
    do
        ./opencl_webcam_OMPP in.avi 10 20 640 480 gauss 10 $i >> filterwidth.txt
        sleep 5
    done
done
echo "GPU" >> filterwidth.txt
for i in 5 9 13;
do
    echo "filterWidth $i" >> filterwidth.txt
    for j in {1..12};
    do
        cat params1.txt | ./opencl_webcam_OCL in.avi 10 20 640 480 gauss 10 256 $i >> filterwidth.txt
        sleep 5
    done
done