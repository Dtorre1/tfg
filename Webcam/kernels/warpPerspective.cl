/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                           License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2010-2012, Institute Of Software Chinese Academy Of Science, all rights reserved.
// Copyright (C) 2010-2012, Advanced Micro Devices, Inc., all rights reserved.
// Third party copyrights are property of their respective owners.
//
// @Authors
//    Zhang Ying, zhangying913@gmail.com
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of the copyright holders may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors as is and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

// #define scalar (T)(scalar_.x, scalar_.y, scalar_.z)
typedef struct T
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
} T;
#define loadpix(addr)  *(__global const T*)(addr)
#define storepix(val, addr)  *(__global T*)(addr) = val
#define pixsize (int)sizeof(T)
#define CT float
__kernel void warpPerspective(__global const uchar * srcptr, __constant CT * M, __global uchar * dstptr, 
                                int src_step, int src_offset, int src_rows, int src_cols,
                                int dst_step, int dst_offset, int dst_rows, int dst_cols)
{


    // int dx = get_global_id(0);
    // int dy = get_global_id(1);
    int index = get_global_id(0);
    int dy = index % src_rows;
    int dx = index / src_rows;

    if (dx < dst_cols && dy < dst_rows)
    {
        CT X0 = M[0] * dx + M[1] * dy + M[2];
        CT Y0 = M[3] * dx + M[4] * dy + M[5];
        CT W = M[6] * dx + M[7] * dy + M[8];
        W = W != 0.f ? 1.f / W : 0.f;
        short sx = convert_short_sat_rte(X0*W);
        short sy = convert_short_sat_rte(Y0*W);
        int dst_index = mad24(dy, dst_step, dx * pixsize + dst_offset);

        if (sx >= 0 && sx < src_cols && sy >= 0 && sy < src_rows)
        {
            int src_index = mad24(sy, src_step, sx * pixsize + src_offset);
            storepix(loadpix(srcptr + src_index), dstptr + dst_index);
        }
        else
        {
            T pix;
            pix.x = 0;
            pix.y = 0;
            pix.z = 0;
            storepix(pix, dstptr + dst_index);
        }
            
    }
}
