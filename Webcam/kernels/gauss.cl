typedef struct pixel_bgr
{
  unsigned char azul;
  unsigned char verde;
  unsigned char rojo;
} pixel_bgr;

__kernel void
gauss(__global pixel_bgr *src, __global float *filter, __global pixel_bgr *dst, int rows, int cols, int filterWidth)
{
    // dst[get_global_id(0)] = src[get_global_id(0)]; //esto funciona bien
    int index = get_global_id(0);
    int numIter = rows * cols;
    if (index < numIter)
    {
        const int width = cols - 1;
        const int height = rows - 1;
        int row = index / cols;
        int col = index % cols;
        int i, j;
        float blur_red = 0;
        float blur_green = 0;
        float blur_blue = 0;
        const int mitad = filterWidth / 2;

        for (i = -mitad; i <= mitad; ++i)
        {
            int h = min(max(row + i, 0), height);
            for (j = -mitad; j <= mitad; ++j)
            {
                // Clamp filter to the image border
                int w = min(max(col + j, 0), width);
                int idx = w + cols * h;                          // current pixel_bgr index
                int idf = (i + mitad) * filterWidth + j + mitad; //current filter index
                float red = (float)(src[idx].rojo);
                float green = (float)(src[idx].verde);
                float blue = (float)(src[idx].azul);
                float weight = filter[idf];
                blur_red += red * weight;
                blur_green += green * weight;
                blur_blue += blue * weight;
            }
        }
        
        dst[index].rojo = (unsigned char)(blur_red);
        dst[index].verde = (unsigned char)(blur_green);
        dst[index].azul = (unsigned char)(blur_blue);
    }
}
