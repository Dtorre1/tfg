typedef struct pixel_bgr
{
  unsigned char azul;
  unsigned char verde;
  unsigned char rojo;
} pixel_bgr;


__kernel void colorMask(__global pixel_bgr *src, __global pixel_bgr *dst, pixel_bgr colorRangeStart, pixel_bgr colorRangeEnd, int rows, int cols)
{
    int k;
    int index = get_global_id(0);
    int numIter = rows * cols;
    if(index < numIter)
    {
        int colors[6];
        bool inRange = true;
        colors[0] = ((int)src[index].rojo) - ((int)colorRangeStart.rojo);
        colors[1] = ((int)colorRangeEnd.rojo) - ((int)src[index].rojo);
        colors[2] = ((int)src[index].verde) - ((int)colorRangeStart.verde);
        colors[3] = ((int)colorRangeEnd.verde) - ((int)src[index].verde);
        colors[4] = ((int)src[index].azul) - ((int)colorRangeStart.azul);
        colors[5] = ((int)colorRangeEnd.azul) - ((int)src[index].azul);
        for (k = 0; k < 6; k++)
        {
            if (colors[k] < 0)
            {
                dst[index].rojo = 0;
                dst[index].verde = 0;
                dst[index].azul = 0;
                k = 6;
                inRange = false;
            }
        }
        if (inRange)
        {
            dst[index].rojo = src[index].rojo;
            dst[index].verde = src[index].verde;
            dst[index].azul = src[index].azul;
        }
    }
    
        
}