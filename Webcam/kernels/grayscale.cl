typedef struct pixel_bgr {
  unsigned char azul;
  unsigned char verde;
  unsigned char rojo;
} pixel_bgr;

__kernel void grayscale(__global pixel_bgr *src, __global pixel_bgr *dst, int rows, int cols) 
{
     int index = get_global_id(0);
     if(index < rows*cols)
     {
          pixel_bgr pix = src[index];
          unsigned char col = (unsigned char)(pix.rojo * 0.3 + pix.verde * 0.59 + pix.azul * 0.11);
          dst[index].rojo = col;
          dst[index].verde = col;
          dst[index].azul = col;
     }
}