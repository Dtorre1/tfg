#!/bin/bash
make OMPP
make OCL
for k in warp;
do
        for j in {1..12};
        do
            sudo ./opencl_webcam_OMPP /dev/video0 10 20 640 480 $k 10 >> resultsFINAL.txt
            sleep 5
        done
done
for k in warp;
do
    for i in paramsGPU.txt paramsCPU.txt;
    do
        echo $i >> resultsFINAL.txt
        for j in {1..12};
        do
            cat $i | sudo ./opencl_webcam_OCL /dev/video0 10 20 640 480 $k 10 12 >> resultsFINAL.txt
            sleep 5
        done
    done
done
