#!/bin/bash
make OCL
echo "CPU" >> videoIn.txt
for i in in.avi /dev/video0;
do
    for k in gauss grayscale;
    do
        for j in {1..12};
        do
            cat params.txt | sudo ./opencl_webcam_OCL $i 10 20 1280 720 $k 10 5 >> videoIn.txt
            sleep 5
        done
    done
done
echo "GPU" >> videoIn.txt
for i in in.avi /dev/video0;
do
    for k in gauss grayscale;
    do
        for j in {1..12};
        do
            cat params1.txt | sudo ./opencl_webcam_OCL $i 10 20 1280 720 $k 10 512 5 >> videoIn.txt
            sleep 5
        done
    done
done