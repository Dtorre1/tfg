#ifndef WEBCAM_FILTERING
#define WEBCAM_FILTERING
    #include <mutex>
    #include <condition_variable>
    #define FUNC_GAUSS 0
    #define FUNC_GRAYSCALE 1
    #define FUNC_WARP 2
    #define FUNC_EDGE 3
    #define SRC_OPENCV 0
    #define SRC_V4L 1

    typedef struct mutual_ex
    {
        std::mutex mutex;
        std::condition_variable frameReady;
        std::condition_variable slotReady;
        int consumerSlot; //next slot to be used by the consumer
        bool done;
        int framesAvailable;
    }mutual_ex;
     /**
     * Function that parses inputted text to generate a number depending
     * on the function parsed
     * Ex: input gauss output 0
     **/
    int getFunctionNumber(char *func_name);
    /**
     * Thread worker function that stores images until is signaled to stop
     * @param size size of the buffer
     * @param buffer Buffer of images to store
     * @param frame_height Number of pixel rows
     * @param frame_width Number of pixel columns
     **/
    void videoStore(int size, pixel_bgr **buffer, int frame_height, int frame_width, int fps, mutual_ex *mutex, char *func);
    void showImageAndFPS(double timePassed, cv::Mat cvFrame, char *func);
#endif