#ifndef FILTERS_H
#define FILTERS_H
#define NAME_GAUSS "kernels/gauss.cl"
#define KFUNC_GAUSS "gauss"
#define NAME_GRAYSCALE "kernels/grayscale.cl"
#define KFUNC_GRAYSCALE "grayscale"
#define NAME_COLORMASK "kernels/colorMask.cl"
#define KFUNC_COLORMASK "colorMask"
/**
 * Creates a gaussian blur filter
 * @param width width of the filter
 */
float *createFilter(int width);
#endif