#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "../bmp.h"
#include "../filters.hpp"
#include "../openCL.h"

void gaussianBlurSetUp(ocl_kernel *kernel, int rows, int cols, float *filter, int filterWidth, int blockSize)
{
    
    int imgSize = rows * cols * sizeof(pixel_bgr);
    int filterSize = filterWidth * filterWidth * sizeof(float);
    kernel->filename = NAME_GAUSS;
    kernel->funcname = KFUNC_GAUSS;

    printMachineInfo();
    fprintf(stderr, "Put platform number: \n");
    scanf("%d",& kernel->platform_num);
    fprintf(stderr, "Put device number: \n");
    scanf("%d",&kernel->device_num);
    kernel->worksizeGL1 = rows*cols;
    kernel->input_num_memObjects = 2;
    kernel->input_memObjects_sizes = (unsigned int *)malloc(2 * sizeof(unsigned int));
    kernel->input_memObjects_sizes[0] = imgSize;
    kernel->input_memObjects_sizes[1] = filterSize;
    kernel->input_memObjects = (cl_mem *)malloc(2 * sizeof(cl_mem));
    kernel->inputLoadCompletion = (cl_event *)malloc(2 * sizeof(cl_event));

    kernel->output_num_memObjects = 1;
    kernel->output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->output_memObjects_sizes[0] = imgSize;
    kernel->output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

    kernel->num_args = 6;
    kernel->argcounter = 0;
    kernel->args = (void **)malloc(6 * sizeof(void *));
    kernel->arg_sizes = (unsigned int *)malloc(6 * sizeof(void *));

    loadKernel(kernel);

    kernel->args[kernel->argcounter] = &rows;
    kernel->arg_sizes[kernel->argcounter] = sizeof(rows);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &cols;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cols);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &filterWidth;
    kernel->arg_sizes[kernel->argcounter] = sizeof(filterWidth);
    loadKernelArgs(kernel);

    int numBlocks = ceil(kernel->input_memObjects_sizes[0] / blockSize);
    kernel->numBlocks = numBlocks;
    kernel->blockSize = blockSize;
}

void gaussianBlur(pixel_bgr *img, float *filter, ocl_kernel *kernel)
{
    loadInputData((char *)img, kernel->input_memObjects_sizes[0], 0, kernel);
    loadInputData((char *)filter, kernel->input_memObjects_sizes[1], 1, kernel);
    executeKernel(kernel);
}

void grayscaleSetUp(ocl_kernel *kernel, int rows, int cols, int blockSize)
{
    int imgSize = rows * cols * sizeof(pixel_bgr);

    kernel->filename = NAME_GRAYSCALE;
    kernel->funcname = KFUNC_GRAYSCALE;

    printMachineInfo();
    fprintf(stderr, "Put platform number: \n");
    scanf("%d",& kernel->platform_num);
    fprintf(stderr, "Put device number: \n");
    scanf("%d",&kernel->device_num);   
    kernel->worksizeGL1 = rows*cols;
    kernel->input_num_memObjects = 1;
    kernel->input_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->input_memObjects_sizes[0] = imgSize;
    kernel->input_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));
    kernel->inputLoadCompletion = (cl_event *)malloc(1 * sizeof(cl_event)); 

    kernel->output_num_memObjects = 1;
    kernel->output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->output_memObjects_sizes[0] = imgSize;
    kernel->output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

    kernel->num_args = 4;
    kernel->argcounter = 0;
    kernel->args = (void **)malloc(4 * sizeof(void *));
    kernel->arg_sizes = (unsigned int *)malloc(4 * sizeof(void *));

    loadKernel(kernel);

    kernel->args[kernel->argcounter] = &rows;
    kernel->arg_sizes[kernel->argcounter] = sizeof(rows);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &cols;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cols);
    kernel->argcounter++;

    loadKernelArgs(kernel);

    int numBlocks = ceil(kernel->input_memObjects_sizes[0] / blockSize);
    kernel->numBlocks = numBlocks;
    kernel->blockSize = blockSize;
}
void grayscale(pixel_bgr *img, ocl_kernel *kernel)
{
    loadInputData((char *)img, kernel->input_memObjects_sizes[0], 0, kernel);
    executeKernel(kernel);
}



void colorMaskSetUp(ocl_kernel *kernel, int rows, int cols, pixel_bgr rangeStart, pixel_bgr rangeEnd, int blockSize)
{
    int imgSize = rows * cols * sizeof(pixel_bgr);

    kernel->filename = NAME_COLORMASK;
    kernel->funcname = KFUNC_COLORMASK;

    printMachineInfo();
    fprintf(stderr, "Put platform number: \n");
    scanf("%d",& kernel->platform_num);
    fprintf(stderr, "Put device number: \n");
    scanf("%d",&kernel->device_num);   
    kernel->worksizeGL1 = rows*cols;
    kernel->input_num_memObjects = 1;
    kernel->input_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->input_memObjects_sizes[0] = imgSize;
    kernel->input_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));
    kernel->inputLoadCompletion = (cl_event *)malloc(1 * sizeof(cl_event)); 

    kernel->output_num_memObjects = 1;
    kernel->output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->output_memObjects_sizes[0] = imgSize;
    kernel->output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

    kernel->num_args = 6;
    kernel->argcounter = 0;
    kernel->args = (void **)malloc(6 * sizeof(void *));
    kernel->arg_sizes = (unsigned int *)malloc(6 * sizeof(void *));

    loadKernel(kernel);

    kernel->args[kernel->argcounter] = &rangeStart;
    kernel->arg_sizes[kernel->argcounter] = sizeof(pixel_bgr);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &rangeEnd;
    kernel->arg_sizes[kernel->argcounter] = sizeof(pixel_bgr);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &rows;
    kernel->arg_sizes[kernel->argcounter] = sizeof(rows);
    kernel->argcounter++;

    kernel->args[kernel->argcounter] = &cols;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cols);
    kernel->argcounter++;

    loadKernelArgs(kernel);

    int numBlocks = ceil(kernel->input_memObjects_sizes[0] / blockSize);
    kernel->numBlocks = numBlocks;
    kernel->blockSize = blockSize;
}

void colorMask(pixel_bgr *img, ocl_kernel *kernel)
{
    loadInputData((char *)img, kernel->input_memObjects_sizes[0], 0, kernel);
    executeKernel(kernel);
}
void kernelFree(ocl_kernel *kernel)
{
    if (kernel == NULL)
    {
        fprintf(stderr, "Error, null pointer");
        return;
    }
    cleanup(kernel);
    free(kernel->input_memObjects_sizes);
    free(kernel->output_memObjects_sizes);
    free(kernel->input_memObjects);
    free(kernel->output_memObjects);
    free(kernel->args);
    free(kernel->arg_sizes);
    free(kernel->inputLoadCompletion);
}

void kernelWaitAndRetrieve(ocl_kernel *kernel, void **destination)
{
    waitForKernel(kernel);
    for (auto i = 0; i < kernel->output_num_memObjects; i++)
    {
        getOutputData((char *)destination[i], kernel->output_memObjects_sizes[i], i, kernel);
    }
}

void warpPerspectiveSetUp(ocl_kernel *kernel, int height, int width, int step)
{
    int imgSize = width * height * sizeof(char) * 3;
    printMachineInfo();
    fprintf(stderr, "Put platform number: \n");
    scanf("%d", &kernel->platform_num);
    fprintf(stderr, "Put device number: \n");
    scanf("%d", &kernel->device_num);
    kernel->worksizeGL1 = width*height;
    kernel->worksizeGL2 = height;
    kernel->blockSize = 12;
    kernel->filename = "kernels/warpPerspective.cl";
    kernel->funcname = "warpPerspective";
    kernel->input_num_memObjects = 2;
    kernel->input_memObjects_sizes = (unsigned int *)malloc(kernel->input_num_memObjects * sizeof(unsigned int));
    kernel->input_memObjects_sizes[0] = imgSize;
    kernel->input_memObjects_sizes[1] = 9 * sizeof(float);
    kernel->input_memObjects = (cl_mem *)malloc(kernel->input_num_memObjects * sizeof(cl_mem));
    kernel->inputLoadCompletion = (cl_event *)malloc(kernel->input_num_memObjects * sizeof(cl_event));

    kernel->output_num_memObjects = 1;
    kernel->output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->output_memObjects_sizes[0] = imgSize;
    kernel->output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

    kernel->num_args = 11;
    kernel->argcounter = 0;
    kernel->args = (void **)malloc(kernel->num_args * sizeof(void *));
    kernel->arg_sizes = (unsigned int *)malloc(kernel->num_args * sizeof(void *));

    loadKernel(kernel);
    int cero = 0;
    kernel->args[kernel->argcounter] = &step;
    kernel->arg_sizes[kernel->argcounter] = sizeof(step);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &cero;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &height;
    kernel->arg_sizes[kernel->argcounter] = sizeof(height);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &width;
    kernel->arg_sizes[kernel->argcounter] = sizeof(width);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &step;
    kernel->arg_sizes[kernel->argcounter] = sizeof(step);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &cero;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &height;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &width;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    loadKernelArgs(kernel);
}

void warpPerspective(ocl_kernel *kernel, char *frame, float *m)
{
    loadInputData((char *)frame, kernel->input_memObjects_sizes[0], 0, kernel);
    loadInputData((char *)m, kernel->input_memObjects_sizes[1], 1, kernel);
    // loadInputData(m, kernel->input_memObjects_sizes[1], 1, kernel);
    executeKernel(kernel);
}