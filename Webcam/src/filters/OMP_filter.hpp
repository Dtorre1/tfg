#ifndef OMP_FILTER_H
#define OMP_FILTER_H

/**
 * Computes a gaussian blur filter
 * @param dst memory space where the resulting frame will be stored
 * @param src frame that will be blurred
 * @param rows frame height
 * @param cols frame width
 * @param filter blur filter to use
 * @param filterWidth width of the blur filter
 */
void gaussianBlur(pixel_bgr *dst, pixel_bgr *src, int rows, int cols, float *filter, int filterWidth);
/**
 * Computes a grayscale filter
 * @param modifiedImage memory space where the image will be stored
 * @param img source frame that will be blurred
 * @param rows frame height
 * @param cols frame width
 */
void grayscale(pixel_bgr *modifiedImage, pixel_bgr *img, int rows, int cols);

void colorMask(pixel_bgr *dst, pixel_bgr *src, pixel_bgr colorRangeStart, pixel_bgr colorRangeEnd, int rows, int cols, bool removeRest);
#endif