#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "../bmp.h"
#include "OMP_filter.hpp"
void gaussianBlur(pixel_bgr *dst, pixel_bgr *src, int rows, int cols, float *filter, int filterWidth)
{
    int row;
    int col;
    int i, j;

    const int half = filterWidth / 2;
    const int width = cols - 1;
    const int height = rows - 1;
    for (row = 0; row < rows; row++)
    {
        for (col = 0; col < cols; col++)
        {
            float blur_red = 0;
            float blur_green = 0;
            float blur_blue = 0;
            for (i = -half; i <= half; ++i)
            {
                int h = std::min(std::max(row + i, 0), height);
                for (j = -half; j <= half; ++j)
                {
                    // Clamp filter to the image border

                    int w = std::min(std::max(col + j, 0), width);
                    int idx = w + cols * h;                        // current pixel index
                    int idf = (i + half) * filterWidth + j + half; //current filter index
                    float red = (float)(src[idx].rojo);
                    float green = (float)(src[idx].verde);
                    float blue = (float)(src[idx].azul);
                    float weight = filter[idf];
                    blur_red += red * weight;
                    blur_green += green * weight;
                    blur_blue += blue * weight;
                }
            }
            dst[row * cols + col].rojo = (unsigned char)(blur_red);
            dst[row * cols + col].verde = (unsigned char)(blur_green);
            dst[row * cols + col].azul = (unsigned char)(blur_blue);
        }
    }
}
void grayscale(pixel_bgr *dst, pixel_bgr *src, int rows, int cols)
{
    int i, j, index;
    unsigned char col;
    pixel_bgr pix;
    for (i = 0; i < rows; i++)
    {
        for (j = 0; j < cols; j++)
        {
            index = i * cols + j;
            pix = src[index];
            col = (unsigned char)(pix.rojo * 0.3 + pix.verde * 0.59 + pix.azul * 0.11);
            dst[index].rojo = col;
            dst[index].verde = col;
            dst[index].azul = col;
        }
    }
}

void colorMask(pixel_bgr *dst, pixel_bgr *src, pixel_bgr colorRangeStart, pixel_bgr colorRangeEnd, int rows, int cols, bool removeRest)
{
    int i, j, k;
    for (i = 0; i < rows; i++)
    {

        for (j = 0; j < cols; j++)
        {
            int index = i * cols + j;
            int colors[6];
            bool inRange = true;
            colors[0] = ((int)src[index].rojo) - ((int)colorRangeStart.rojo);
            colors[1] = ((int)colorRangeEnd.rojo) - ((int)src[index].rojo);
            colors[2] = ((int)src[index].verde) - ((int)colorRangeStart.verde);
            colors[3] = ((int)colorRangeEnd.verde) - ((int)src[index].verde);
            colors[4] = ((int)src[index].azul) - ((int)colorRangeStart.azul);
            colors[5] = ((int)colorRangeEnd.azul) - ((int)src[index].azul);
            for (k = 0; k < 6; k++)
            {
                if (colors[k] < 0)
                {
                    dst[index].rojo = 0;
                    dst[index].verde = 0;
                    dst[index].azul = 0;
                    k = 6;
                    inRange = false;
                }
            }
            if (inRange)
            {
                dst[index].rojo = src[index].rojo;
                dst[index].verde = src[index].verde;
                dst[index].azul = src[index].azul;
            }
        }
    }
}