#ifndef OCL_FILTER_H
#define OCL_FILTER_H
#include "../openCL.h"

/**
 * Executes previously set up gaussian blur OpenCL kernel in a NON BLOCKING WAY
 * @param img input frame
 * @param filter input gauss filter
 * @param kernel struct containing OpenCL kernel config and data
 */
void gaussianBlur(pixel_bgr *img, float *filter, ocl_kernel *kernel);
/**
 * Executes previously set up grayscale OpenCL kernel in a NON BLOCKING WAY
 * @param img input frame
 * @param kernel struct containing OpenCL kernel config and data
 */
void grayscale(pixel_bgr *img, ocl_kernel *kernel);
/**
 * Sets up gaussianBlur OpenCL kernel
 * @param kernel struct containing OpenCL kernel config and data
 * @param rows height of frames used
 * @param cols width of frames used
 * @param filter blur filter
 * @param filterWidth width of the filter
 * @param block_size size of OpenCL worker blocks
 */
void gaussianBlurSetUp(ocl_kernel *kernel, int rows, int cols, float *filter, int filterWidth, int blockSize);
/**
 * Sets up grayscale OpenCL kernel
 * @param kernel struct containing OpenCL kernel config and data
 * @param rows height of frames used
 * @param cols width of frames used
 * @param block_size size of OpenCL worker blocks
 */
void grayscaleSetUp(ocl_kernel *kernel, int rows, int cols, int blockSize);
void colorMaskSetUp(ocl_kernel *kernel, int rows, int cols, pixel_bgr rangeStart, pixel_bgr rangeEnd, int blockSize);
void colorMask(pixel_bgr *img, ocl_kernel *kernel);
/**
 * Frees kernel structure and its data
 * @param kernel kernel struct to free
 */
void kernelFree(ocl_kernel *kernel);
/**
 * Wait for kernel execution to complete and retrieves the data
 * @param kernel kernel to wait for
 * @param destination array of pointers where data will be saved, one pointer for each output queue
 */
void kernelWaitAndRetrieve(ocl_kernel *kernel, void **destination);
void warpPerspective(ocl_kernel *kernel, char *frame, float *m);
void warpPerspectiveSetUp(ocl_kernel *kernel, int height, int width, int step);
#endif