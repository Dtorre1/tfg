#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <thread>
#include <omp.h>
#include <math.h>
#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include "opencv2/imgproc.hpp"
#ifdef SCREEN
#include "opencv2/highgui/highgui.hpp"
#endif
#include "webcam_capture.hpp"
#include "bmp.h"
#include "filters.hpp"
#include "webcam_filtering.hpp"
#ifdef OCL
    #include "filters/OCL_filter.hpp"
    #include "openCL.h"
#endif
#if defined(OMPP) || defined(OMPT) || defined(SEQU)
    #include "filters/OMP_filter.hpp"
#endif


int main(int argc, char **argv)
{
    #if (defined(OCL) && defined(OMPT)) || (defined(OCL) && defined(OMPP)) || (defined(OMPT) && defined(OMPP))
        std::cerr << "WRONG COMPILATION: Only one of the following flags may be activated during compilation: OCL OMPT OMPP" << std::endl;
        exit(1);
    #endif
    #ifdef OCL
        char *version = "OCL";
    #endif
    #ifdef OMPT
        char *version = "OMPT";
    #endif
    #ifdef OMPP
        char *version = "OMPP";
    #endif
    #ifdef SEQU
        char *version = "SEQU";
    #endif
    #ifdef SCREEN
        struct timeval displayStart, displayEnd;
        double timePassed;
    #endif
    //profiling
    struct timeval timer_start, timer_end, frame_start, frame_end, func_start, func_end, write_start, write_end;
    double compute_time, ini_time, func_time, write_time;
    double frame_time = 0.0;
    gettimeofday(&timer_start, NULL);
    #ifdef OCL
        int expectedArgs = 9;
    #endif
    #ifndef OCL
        int expectedArgs = 8;
    #endif
    if (argc < expectedArgs)
    {
        #ifdef OCL
            std::cerr << "USAGE ./webcam <videoSource> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> <block_size> [gauss_width]" << std::endl;
        #endif
        #ifndef OCL
            std::cerr << "USAGE ./webcam </dev/videoN> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> [gauss_width]" << std::endl;
        #endif
        std::cerr << "For filter use gauss or grayscale and for gauss" << std::endl;
        std::cerr << "Buffer_size is measured in frames ex: 5 frame buffer" << std::endl;
        exit(1);
    }
    
    char *devname = argv[1];
    int frame_width = atoi(argv[4]);
    int frame_height = atoi(argv[5]);
    int t_sec = atoi(argv[2]);
    int fps = atoi(argv[3]);
    if(t_sec <= 0 || fps <= 0)
    {
        std::cerr << "Wrong video parameters, FPS and time cannot be negative or zero" << std::endl;
        exit(1);
    }
    int num_frames = t_sec * fps;
    char *funcname = argv[6];
    int filterWidth = 5;
    int function = getFunctionNumber(funcname);
    if(function == FUNC_GAUSS)
    {
        if(argc < expectedArgs + 1)
        {
            std::cerr << "No gauss/filter width param given using 5" << std::endl;

        }
        else
        {
            #ifdef OCL
                filterWidth = atoi(argv[9]);
                fprintf(stderr, "FilterWidth %d\n", filterWidth);
            #endif
            #ifndef OCL
                filterWidth = atoi(argv[8]);
            #endif
        }   
    }
    int buffer_size = atoi(argv[7]);
    float *filter;
    if(function == FUNC_GAUSS || function == FUNC_EDGE)
    {
        filter = createFilter(filterWidth);
    }
    int source = SRC_V4L;
    if(strncmp(devname,"/dev/video",10) != 0)
    { 
        source = SRC_OPENCV;
    }
    #ifdef OCL
        int block_size = atoi(argv[8]);
        if(block_size <= 0)
        {
            std::cerr << "Block_size cannot be zero or lower" << std::endl;
            exit(1);
        }
        ocl_kernel kernel;
        switch (function)
        {
        case FUNC_GAUSS:
            gaussianBlurSetUp(&kernel, frame_height, frame_width, filter, filterWidth, block_size);
            break;
        case FUNC_GRAYSCALE:
            grayscaleSetUp(&kernel, frame_height, frame_width, block_size);
            break;
        case FUNC_WARP:
        {
            cv::Mat frame;
            cv::VideoCapture inputVideo(devname);
            if(SRC_V4L)
            {
                inputVideo.set(cv::CAP_PROP_FRAME_WIDTH, frame_width);
                inputVideo.set(cv::CAP_PROP_FRAME_HEIGHT, frame_height);
            }
            inputVideo >> frame;
            warpPerspectiveSetUp(&kernel, frame_height, frame_width, frame.step1());
            inputVideo.release();
            break;
        }
        case FUNC_EDGE:
            fprintf(stderr, "Edge filter\n");
            break;
        default:
            fprintf(stderr, "No filter selected\n");
            break;
        }
    #endif
    
    
    
    cv::VideoCapture inputVideo;
    if(source == SRC_OPENCV)
    {
        fprintf(stderr, "Videofile selected \n");
        inputVideo = cv::VideoCapture(devname);
        frame_width = (int) inputVideo.get(cv::CAP_PROP_FRAME_WIDTH);
        frame_height = (int) inputVideo.get(cv::CAP_PROP_FRAME_HEIGHT);
        num_frames = (int) inputVideo.get(cv::CAP_PROP_FRAME_COUNT);
    }
    else
    {
        cameraInit(devname, frame_width, frame_height);
    }
    #ifdef OMPT
        if (buffer_size <= 0 || num_frames % buffer_size != 0)
        {
            std::cerr << "Buffer cannot be lower than 1 and must be a divisor of num_frames" << std::endl;
            v4l2_close();
            exit(1);
        }
    #endif
    #if defined(OMPT) || defined(SEQU)
        cv::VideoWriter video("outcpp_blurred.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
    #endif
    int imgSize = frame_height * frame_width;
    cv::Mat *videofileBuffer;//for reading video from file

    char **frame_buffer = (char **)malloc(buffer_size * sizeof(char *)); //will contain the frames 
    pixel_bgr **blurred_frame = (pixel_bgr **)malloc(buffer_size * sizeof(pixel_bgr *));
    pixel_bgr **store_blurred = (pixel_bgr **)malloc(buffer_size * sizeof(pixel_bgr *)); //auxiliar buffers to avoid blocking on write operations
    if(source == SRC_OPENCV)
    {
        videofileBuffer = new cv::Mat[buffer_size];
    }
    for (auto i = 0; i < buffer_size; i++)
    {
        if(source == SRC_V4L)
            frame_buffer[i] = (char *)malloc(imgSize * sizeof(pixel_bgr));
        blurred_frame[i] = (pixel_bgr *)malloc(imgSize * sizeof(pixel_bgr));
        store_blurred[i] = (pixel_bgr *)malloc(imgSize * sizeof(pixel_bgr));
    }
    cv::Mat transform;
    float transformReal[9];
    if(function == FUNC_WARP)
    {
        std::vector<cv::Point2f> points1, points2;
        points1.push_back(cv::Point2f(10, 10));
        points1.push_back(cv::Point2f(300, 100));
        points1.push_back(cv::Point2f(10, 400));
        points1.push_back(cv::Point2f(300, 450));
        points2.push_back(cv::Point2f(10, 10));
        points2.push_back(cv::Point2f(300, 10));
        points2.push_back(cv::Point2f(10, 400));
        points2.push_back(cv::Point2f(300, 400));
        transform = getPerspectiveTransform(points1, points2);
        cv::invert(transform, transform);
        double *array2 = (double *) transform.data;
        for(auto i=0; i<9;i++)
        {
            transformReal[i] = (float) array2[i];
            // fprintf(stderr, "matriz %f %f\n", transformReal[i], array2[i]);
        }
        transform.data = (unsigned char*)transformReal;
    }
    gettimeofday(&timer_end, NULL);
    ini_time = timer_difference(timer_end, timer_start);
    gettimeofday(&timer_start, NULL);
    int num_buffers = num_frames / buffer_size; //every buffer_size number of frames, a write operation is issued
    #if defined(OMPT)
        #pragma omp parallel num_threads(4)             //using pipeline to avoid blocking the program on disk writes
        {
            #pragma omp single
            {
                for (auto j = 0; j < num_buffers; j++)
                {
                    for (auto i = 0; i < buffer_size; i++)
                    {
                        gettimeofday(&frame_start, NULL);
                        #ifdef SCREEN
                            gettimeofday(&displayStart, NULL);
                        #endif
                        /**
                         * Grabbing frame from appropiate source
                         **/
                        switch(source)
                        {
                            case SRC_OPENCV:
                                inputVideo >> videofileBuffer[i]; 
                                frame_buffer[i] = (char *)videofileBuffer[i].data;
                                break;
                            case SRC_V4L:
                                grabFrame(frame_buffer[i]); 
                                break; 
                            default:
                                std::cerr << "Error, no frame grabber selected" << std::endl;
                                printf("Source: %d\n");
                                exit(1);
                        }
                        gettimeofday(&frame_end, NULL);
                        frame_time += timer_difference(frame_end, frame_start);
                        #pragma omp task
                        {
                            switch(function)
                            {
                                case FUNC_GAUSS:
                                    gaussianBlur(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width, filter, filterWidth);
                                    break;
                                case FUNC_GRAYSCALE:
                                    grayscale(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width);
                                    break;
                                case FUNC_WARP:
                                {
                                    cv::Mat frame(frame_height, frame_width, CV_8UC3, frame_buffer[i]);
                                    frame.data = (uchar *) frame_buffer[i];
                                    cv::Mat rotated(frame_height, frame_width, CV_8UC3, blurred_frame[i]);
                                    rotated.data = (uchar *) blurred_frame[i];
                                    cv::warpPerspective(frame, rotated, transform, frame.size());
                                    break;
                                }
                                default:
                                    std::memcpy(blurred_frame[i], frame_buffer[i], imgSize * sizeof(pixel_bgr));
                                    break;
                            } 
                        }
                        
                    }
                    #pragma omp taskwait //wait for the previous write operation to finish
                    //move pointers around to avoid blocking
                    pixel_bgr **aux2 = blurred_frame;
                    blurred_frame = store_blurred;
                    store_blurred = aux2;
                    #pragma omp task
                    {
                        for (auto i = 0; i < buffer_size; i++)
                        {
                            //adapt to opencv and let it handle the write
                            cv::Mat cvFrame(frame_height, frame_width, CV_8UC3, store_blurred[i]);
                            cvFrame.data = (uchar *) store_blurred[i];
                            video.write(cvFrame);
                            #ifdef SCREEN
                                gettimeofday(&displayEnd,NULL);
                                showImageAndFPS(timer_difference(displayEnd,displayStart), cvFrame, funcname);
                            #endif       
                        }
                    }
                }
            }
        }
    #endif

    #if defined(OMPP) || defined(OCL)
        bool firstIter = true;
        mutual_ex mutex;
        mutex.consumerSlot = 0;
        mutex.framesAvailable = 0;
        mutex.done = false;
        std::thread videoStoreWorker(videoStore, buffer_size, blurred_frame, frame_height, frame_width, fps, &mutex, funcname);
       
        for (auto j = 0; j < num_buffers; j++)
        {
            for (auto i = 0; i < buffer_size; i++)
            {             
                gettimeofday(&frame_start, NULL);
                /**
                 * Grabbing frame from appropiate source
                 **/
                switch(source)
                {
                    case SRC_OPENCV:
                        inputVideo.read(videofileBuffer[i]); //video is a global variable
                        frame_buffer[i] = (char *) videofileBuffer[i].data;                       
                        break;
                    case SRC_V4L:
                        grabFrame(frame_buffer[i]);
                        break; 
                    default:
                        std::cerr << "Error, no frame grabber selected" << std::endl;
                        printf("Source: %d\n");
                        exit(1);
                }
                gettimeofday(&frame_end, NULL);
                frame_time += timer_difference(frame_end, frame_start);
                #ifdef OCL
                    if(!firstIter)
                    {
                        std::unique_lock<std::mutex> lk(mutex.mutex);
                        if(mutex.framesAvailable == buffer_size)
                        {
                            mutex.slotReady.wait(lk);
                        }
                        if(function != -1)
                        {
                            kernelWaitAndRetrieve(&kernel, (void **) &blurred_frame[i]);
                        }
                        mutex.framesAvailable++;
                        mutex.frameReady.notify_all();
                        lk.unlock();
                    }
                    firstIter = false;
                    gettimeofday(&func_start, NULL);
                    switch (function)
                    {
                    case FUNC_GAUSS: //gaussian blur
                        gaussianBlur((pixel_bgr *)frame_buffer[i], filter, &kernel);
                        break;
                    case FUNC_GRAYSCALE:
                        grayscale((pixel_bgr *)frame_buffer[i], &kernel);
                        break;
                    case FUNC_WARP:
                    {
                        warpPerspective(&kernel, frame_buffer[i], transformReal);
                        break;   
                    }
                    default: //record and store only
                        std::memcpy(blurred_frame[i], frame_buffer[i], imgSize * sizeof(pixel_bgr));
                        break;
                    }
                    gettimeofday(&func_end, NULL);
                    func_time += timer_difference(func_end, func_start);
                #endif
                #ifdef OMPP
                    std::unique_lock<std::mutex> lk(mutex.mutex);
                    if(mutex.framesAvailable == buffer_size)
                    {
                        mutex.slotReady.wait(lk);
                    }
                    lk.unlock();
                    switch(function)
                    {
                        case FUNC_GAUSS:
                            gaussianBlur(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width, filter, filterWidth);
                            break;
                        case FUNC_GRAYSCALE:
                            grayscale(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width);
                            break;
                        case FUNC_WARP:
                        {
                            cv::Mat frame(frame_height, frame_width, CV_8UC3, frame_buffer[i]);
                            frame.data = (uchar *) frame_buffer[i];
                            cv::Mat rotated(frame_height, frame_width, CV_8UC3, blurred_frame[i]);
                            rotated.data = (uchar *) blurred_frame[i];
                            cv::warpPerspective(frame, rotated, transform, frame.size());  
                            break;
                        }
                        case FUNC_EDGE:
                        {
                            cv::Mat frame(frame_height, frame_width, CV_8UC3, frame_buffer[i]);
                            frame.data = (uchar *) frame_buffer[i];
                            cv::Mat edges;
                            edges.create(frame.size(), frame.type());
                            edges.data = (uchar *) blurred_frame[i];
                            gaussianBlur((pixel_bgr *)edges.data, (pixel_bgr *)frame.data, frame_height, frame_width, filter, 5);
                            cv::cvtColor(frame, edges, cv::COLOR_BGR2GRAY);
                            cv::Canny(edges, edges, 100.0, 300.0);
                            cv::cvtColor(edges, frame, cv::COLOR_GRAY2BGR);
                            std::memcpy(blurred_frame[i], frame.data, imgSize * sizeof(pixel_bgr));
                            break;
                        }
                        default:
                            std::memcpy(blurred_frame[i], frame_buffer[i], imgSize * sizeof(pixel_bgr));
                            break;
                    }
                    lk.lock();
                    mutex.framesAvailable++;
                    mutex.frameReady.notify_all();
                    lk.unlock(); 
                #endif
            } 
        }
        std::unique_lock<std::mutex> lk(mutex.mutex);
        mutex.done = true;
        lk.unlock();
        videoStoreWorker.join();                    
    #endif
    #if defined(SEQU)
        
        for (auto j = 0; j < num_buffers; j++)
        {
            for (auto i = 0; i < buffer_size; i++)
            {
                gettimeofday(&frame_start, NULL);
                #ifdef SCREEN
                    gettimeofday(&displayStart, NULL);
                #endif
                /**
                 * Grabbing frame from appropiate source
                 **/
                switch(source)
                {
                    case SRC_OPENCV:
                        inputVideo >> videofileBuffer[i]; 
                        frame_buffer[i] = (char *)videofileBuffer[i].data;
                        break;
                    case SRC_V4L:
                        grabFrame(frame_buffer[i]); 
                        break;
                    default:
                        std::cerr << "Error, no frame grabber selected" << std::endl;
                        printf("Source: %d\n");
                        exit(1);
                }
                gettimeofday(&frame_end, NULL);
                frame_time += timer_difference(frame_end, frame_start);
                
                switch(function)
                {
                    case FUNC_GAUSS:
                        gaussianBlur(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width, filter, filterWidth);
                        break;
                    case FUNC_GRAYSCALE:
                        grayscale(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width);
                        break;
                    case FUNC_WARP:
                    {
                        cv::Mat frame(frame_height, frame_width, CV_8UC3, frame_buffer[i]);
                        frame.data = (uchar *) frame_buffer[i];
                        cv::Mat rotated(frame_height, frame_width, CV_8UC3, blurred_frame[i]);
                        rotated.data = (uchar *) blurred_frame[i];
                        cv::warpPerspective(frame, rotated, transform, frame.size());
                        break;
                    }
                    default:
                        std::memcpy(blurred_frame[i], frame_buffer[i], imgSize * sizeof(pixel_bgr));
                        break;
                }
                cv::Mat cvFrame(frame_height, frame_width, CV_8UC3, blurred_frame[i]);        
                cvFrame.data = (uchar *) blurred_frame[i];
                video.write(cvFrame);   
                #ifdef SCREEN
                    gettimeofday(&displayEnd,NULL);
                    showImageAndFPS(timer_difference(displayEnd,displayStart), cvFrame, funcname);
                #endif    
            }
            
        }   
    #endif
    gettimeofday(&timer_end, NULL);
    compute_time = timer_difference(timer_end, timer_start);
    #ifdef OMPT
        video.release();
    #endif
    if(source == SRC_OPENCV)
    {
        inputVideo.release();
    }
    else
    {
        v4l2_close();
    }
    #ifdef OCL
        if(function != -1)
        {
            kernelFree(&kernel);
        }
    #endif
    for (auto i = 0; i < buffer_size; i++)
    {
        if(source == SRC_V4L)
            free(frame_buffer[i]);
        free(blurred_frame[i]);
        free(store_blurred[i]);
    }
    free(frame_buffer);
    free(blurred_frame);
    free(store_blurred);
    printf("%d:%d:%d:%s:buffer-%d:%f:record+%s:%f\n",
           frame_width, frame_height, num_frames, version, buffer_size, compute_time, 
           funcname, ini_time);
    fflush(stdout);
    return 0;
}

int getFunctionNumber(char *func_name)
{
    if (strcmp("gauss", func_name) == 0)
    {
        return FUNC_GAUSS;
    }
    if (strcmp("grayscale", func_name) == 0)
    {
        return FUNC_GRAYSCALE;
    }
    if(strcmp("warp",func_name) == 0)
    {
        return FUNC_WARP;
    }
    if(strcmp("edge",func_name) == 0)
    {
        return FUNC_EDGE;
    }
    return -1;
}
#if defined(OCL) || defined(OMPP)
    void videoStore(int size, pixel_bgr **buffer, int frame_height, int frame_width, int fps, mutual_ex *mutex, char *func)
    { 
        struct timeval timerStart;
        struct timeval timerEnd;
        double calculatedFPS;
        cv::VideoWriter video("out.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
        int imageSize = frame_height * frame_width * sizeof(pixel_bgr);
        while (true)
        {
            #ifdef SCREEN
                gettimeofday(&timerStart, NULL);
            #endif
            std::unique_lock<std::mutex> lk(mutex->mutex);
            if(mutex->done && mutex->framesAvailable == 0) //when main thread is done and there are no frames remaining
            {
                video.release();
                return;
            }
            while(mutex->framesAvailable == 0)
            {
                mutex->frameReady.wait(lk);
            }
            int index = mutex->consumerSlot;
            lk.unlock();
            cv::Mat cvFrame(frame_height, frame_width, CV_8UC3, buffer[index]);
            cvFrame.data = (uchar *) buffer[index];
            video.write(cvFrame);
            #ifdef SCREEN
                gettimeofday(&timerEnd, NULL);
                showImageAndFPS(timer_difference(timerEnd,timerStart), cvFrame, func);
            #endif 
            lk.lock();
            mutex->consumerSlot = (mutex->consumerSlot + 1) % size;
            mutex->framesAvailable--;
            mutex->slotReady.notify_all();
            lk.unlock();
              
        }
    }
#endif
#ifdef SCREEN
    void showImageAndFPS(double timePassed, cv::Mat cvFrame, char *func)
    {
        int fps = floor(1 / timePassed);
        cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
        char text[8];
        char text2[30];
        sprintf(text,"FPS:%d", fps);
        cv::putText(cvFrame, text, cv::Point(0,30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
        #ifdef OCL
            sprintf(text2,"OCL - %s", func);
            cv::putText(cvFrame, text2, cv::Point(0, cvFrame.rows - 30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
        #endif
        #ifdef OMPP
            sprintf(text2,"OMP Parallel - %s", func);
            cv::putText(cvFrame, text2, cv::Point(0, cvFrame.rows - 30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
        #endif
        #ifdef OMPT
            sprintf(text2,"OMP Tasks - %s", func);  
            cv::putText(cvFrame, text2, cv::Point(0, cvFrame.rows - 30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
        #endif
        #ifdef SEQU
            sprintf(text2,"Sequential - %s", func);
            cv::putText(cvFrame, text2, cv::Point(0, cvFrame.rows - 30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
        #endif
        cv::imshow("Display window", cvFrame);
        cv::waitKey(1);  
    }
#endif
