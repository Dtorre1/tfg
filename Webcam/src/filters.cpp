#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "bmp.h"
#include "filters.hpp"

float *createFilter(int width)
{
    const float sigma = 2.f; // Standard deviation of the Gaussian distribution.

    const int half = width / 2;
    float sum = 0.f;

    // Create convolution matrix
    float *res = (float *)malloc(width * width * sizeof(float));

    // Calculate filter sum first
    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            // e (natural logarithm base) to the power x, where x is what's in the brackets
            float weight = expf((float)(c * c + r * r) / (2.f * sigma * sigma));
            int idx = (r + half) * width + c + half;

            res[idx] = weight;
            sum += weight;
        }
    }

    // Normalize weight: sum of weights must equal 1
    float normal = 1.f / sum;

    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            int idx = (r + half) * width + c + half;

            res[idx] *= normal;
        }
    }
    return res;
}
