#!/bin/bash
make SIMD
make OMPP
for i in opencl_webcam_SIMD opencl_webcam_OMPP;
do
    echo $i >> results.txt
    for j in {1..12};
    do
        ./$i /dev/video0 10 20 640 480 gauss 10 >> results.txt
        sleep 5
    done
done