#!/bin/bash
make OCL
make OMPP
make OMPT
for i in OMPP OMPT;
do
    for k in gauss grayscale nofil;
    do
        for j in {1..12};
        do
            cat params.txt | sudo ./opencl_webcam_$i in.avi 10 20 1280 720 $k 10 5 >> videoIn.txt
            sleep 5
        done
    done
done
for k in gauss grayscale nofil;
do
    for j in {1..12};
    do
        cat params.txt | sudo ./opencl_webcam_$i in.avi 10 20 1280 720 $k 10 512 5 >> videoIn.txt
        sleep 5
    done
done

