#!/bin/bash
echo "CPU" >> videoLength.txt
for i in 5 10 15;
do
    for k in gauss grayscale;
    do
        for j in {1..12};
        do
            cat params.txt | sudo ./opencl_webcam_OCL /dev/video0 $i 20 640 480 $k 10 4 >> videoLength.txt
            sleep 5
        done
    done
done
echo "GPU" >> videoLength.txt
for i in 5 10 15;
do
    for k in gauss grayscale;
    do
        for j in {1..12};
        do
            cat params1.txt | sudo ./opencl_webcam_OCL /dev/video0 $i 20 640 480 $k 10 12 5 >> videoLength.txt
            sleep 5
        done
    done
done