# USAGE:
Compiling
- For Sequential version: `make SEQ`
- For Data parallel     : `make OMPP`
- For Task              : `make OMPT`
- For OpenCL            : `make OCL` or `make`
To use the screen simply add `SCREEN=yes` after desired target
Example: `make OMPP SCREEN=yes`

Executing
- OpenCL: `./opencl_webcam_OCL <videoSource> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> <block_size> [gauss_width]`
- Sequential: `./opencl_webcam_SEQ <videoSource> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> [gauss_width]`
- OpenMP Tasks: `./opencl_webcam_OMPT <videoSource> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> [gauss_width]`
- OpenMP Data paralell: `./opencl_webcam_OMPP <videoSource> <num_seconds> <FPS> <width> <height> <filter> <buffer_size> [gauss_width]`

Filters for <filter>:
- gauss
- grayscale
- warp
- other strings will parsed as "no filter"

Videosource can be a webcam (using `/dev/video`) or a videofile.
gauss_width is optional and only used when <filter> is gauss

For running specifics tests simply execute the corresponding bash script.
