

int ancho, alto;
char *fotograma;
...
cv::VideoWriter video("salida.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 30, cv::Size(ancho, alto));
...
cv::Mat fotogramaCV(ancho, alto, CV_8UC3, fotograma);
fotogramaCV.data = fotograma;
video.write(fotogramaCV);
video.release();