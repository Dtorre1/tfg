void parallelAddition(unsigned N, const double *A, const double *B, double *C)
{
    unsigned i;

    #pragma omp parallel for shared(A, B, C, N) private(i) schedule(static)
    for (i = 0; i < N; ++i)
    {
        C[i] = A[i] + B[i];
    }
}

void parallelAddition(unsigned N, const double *A, const double *B, double *C)
{
    unsigned i;

    #pragma omp parallel shared(A, B, C, N) private(i)
    {
        for (i = 0; i < N; ++i)
        {
            #pragma omp task
            {
                C[i] = A[i] + B[i];
            }
        }
    #pragma omp taskwait
    }
}

#pragma omp simd
for (auto i = 0; i < count; i++)
{
    a[i] = a[i - 1] + 1;
    b[i] = *c + 1;
}

int i;
float a[10], b[10], c[10];
for(i = 0; i< 10; i++)
{
    c[i] = a[i] + b[i]
}

__kernel void vectorSum(__global float *a, __global float *b, __global float *c, int size)
{
    int index = get_global_id(0); //get thread id
    if(index < size)
    {
        c[index] = a[index] + b[index];
    }
}