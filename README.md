# TFG

## Commits importantes:
-  OpenMP con estrategia de buffer usando OpenCV d2312fb2fc6dfa382baf5cd01fd6b988abf307eb
-  OpenMP con V4l2 091ddd86eac394370754fd060bca2e2a7f54457b


## Reproducir experimentos:

- En el directorio TFG/Webcam se encuentra el programa principal sujeto a multitud
de experimentos. 

- En el directorio profiling/tests se encuentran varios tests como la 
medición de rendimiento de opencv, v4l2 etc.

- En el directorio Hello World se encuentra el programa inicial del proyecto.