/* V4L2 video picture grabber
   Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include "/usr/include/libv4l2.h"
#include "bmp.h"
#include "webcam_capture.hpp"
#define CLEAR(x) memset(&(x), 0, sizeof(x))
using namespace std;
struct buffer
{
        void *start;
        size_t length;
};
/**
 * variables
 * */
struct v4l2_format fmt;
struct v4l2_buffer buf;
struct v4l2_requestbuffers req;
enum v4l2_buf_type type;
fd_set fds;
struct timeval tv;
int r, fd = -1;
unsigned int i, n_buffers;

char out_name[256];
FILE *fout;
struct buffer *buffers;

static void xioctl(int fh, int request, void *arg)
{
        int r;

        do
        {
                r = v4l2_ioctl(fh, request, arg);
        } while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

        if (r == -1)
        {
                fprintf(stderr, "error %d, %s\n", errno, strerror(errno));
                exit(EXIT_FAILURE);
        }
}
int cameraInit(char *dev_name, int width, int height)
{

        fd = v4l2_open(dev_name, O_RDWR | O_NONBLOCK, 0);
        if (fd < 0)
        {
                perror("Cannot open device");
                exit(EXIT_FAILURE);
        }
        CLEAR(fmt);
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width = width;
        fmt.fmt.pix.height = height;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        fmt.fmt.pix.field = V4L2_FIELD_ANY;
        xioctl(fd, VIDIOC_S_FMT, &fmt);
        if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_YUYV)
        {
                printf("Libv4l didn't accept RGB24 format. Can't proceed.\n");
                exit(EXIT_FAILURE);
        }
        if ((fmt.fmt.pix.width != width) || (fmt.fmt.pix.height != height))
                printf("Warning: driver is sending image at %dx%d\n", fmt.fmt.pix.width, fmt.fmt.pix.height);

        CLEAR(req);
        req.count = 2;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        xioctl(fd, VIDIOC_REQBUFS, &req);
        buffers = (buffer *)calloc(req.count, sizeof(*buffers));
        for (n_buffers = 0; n_buffers < req.count; ++n_buffers)
        {
                CLEAR(buf);

                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index = n_buffers;

                xioctl(fd, VIDIOC_QUERYBUF, &buf);

                buffers[n_buffers].length = buf.length;
                buffers[n_buffers].start = v4l2_mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);

                if (MAP_FAILED == buffers[n_buffers].start)
                {
                        perror("mmap");
                        exit(EXIT_FAILURE);
                }
        }

        for (i = 0; i < n_buffers; ++i)
        {
                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index = i;
                xioctl(fd, VIDIOC_QBUF, &buf);
        }

        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_STREAMON, &type);


        return 0;
}
int grabFrame(char *dest)
{       
        do
        {
                FD_ZERO(&fds);
                FD_SET(fd, &fds);

                /* Timeout. */
                tv.tv_sec = 2;
                tv.tv_usec = 0;
                r = select(fd + 1, &fds, NULL, NULL, &tv);

        } while ((r == -1 && (errno = EINTR)));
        if (r == -1)
        {
                perror("select");
                return errno;
        }
        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        xioctl(fd, VIDIOC_DQBUF, &buf);
        
        
        memcpy(dest, buffers[buf.index].start, buf.bytesused);        
        xioctl(fd, VIDIOC_QBUF, &buf);

        return 0;
}

void v4l2_close()
{
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_STREAMOFF, &type);
        for (i = 0; i < n_buffers; ++i)
        {
                v4l2_munmap(buffers[i].start, buffers[i].length);
        }
        v4l2_close(fd);
}
/**
 * Calculates the time passed between two timestamps
 * */
double timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
        return ((double)(t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec) / 1000000.0;
}

pixel *flipImageAndColors(pixel *img, int height, int width)
{
        pixel *result = (pixel *)malloc(height * width * 3 * sizeof(char));
        int i, j, color;
        for (i = 0; i < height; i++)
        {
                for (j = 0; j < width; j++)
                {
                        result[i * width + j].red = img[(height - i) * width + j].blue;
                        result[i * width + j].blue = img[(height - i) * width + j].red;
                        result[i * width + j].green = img[(height - i) * width + j].green;
                }
        }
        return result;
}


int storeImage(char *filename, bmpInfoHeader *infoHeader, bmpFileHeader *fileHeader, char *data)
{
    FILE *file;
    unsigned char *img;
    int check;

    file = fopen(filename, "w");
    if (file == NULL)
    {
        return -1;
    }

    uint16_t type = IMAGE_FORMAT;
    check = fwrite(&type, sizeof(uint16_t), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar el formato de imagen\n");
        fclose(file);
        return -1;
    }

    check = fwrite(fileHeader, sizeof(bmpFileHeader), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar fileHeader\n");
        fclose(file);
        return -1;
    }
    check = fwrite(infoHeader, sizeof(bmpInfoHeader), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar infoHeader\n");
        fclose(file);
        return -1;
    }

    fwrite(data, infoHeader->imgsize, 1, file);
    free(data);
    fclose(file);
    return 0;
}



/**
 *  Prepares BMP structs and saves the img as "filename"
 * */
void storeImageHelper(char *filename, char *imgSrc, int width, int height)
{
        char *resultado = (char *)flipImageAndColors((pixel *)imgSrc, height, width);
        bmpInfoHeader infoHeader;
        infoHeader.bpp = BMP_BPP;
        infoHeader.colors = BMP_COLORS;
        infoHeader.imxtcolors = BMP_IMPORTANT_COLORS;
        infoHeader.compress = BMP_COMPRESSION;
        infoHeader.planes = BMP_PLANES;
        infoHeader.headersize = BMP_HEADER_SIZE;
        infoHeader.bpmx = BMP_BPM;
        infoHeader.bpmy = BMP_BPM;
        infoHeader.height = height;
        infoHeader.width = width;
        infoHeader.imgsize = width * height * sizeof(pixel);
        bmpFileHeader fileHeader;
        fileHeader.size = infoHeader.imgsize + BMP_SIZE;
        fileHeader.offset = BMP_OFFSET;
        char actualFilename[strlen(filename) + 4];
        sprintf(actualFilename, "%s.bmp", filename);
        storeImage(actualFilename, &infoHeader, &fileHeader, resultado);
}

