#ifndef WEBCAM_CAPTURE_H
#define WEBCAM_CAPTURE_H
#include "bmp.h"
/**
 * Functions
 * */
static void xioctl(int fh, int request, void *arg);
/**
 * Function to store a raw frame as BMP file
 * @param filename filename to use
 * @param rgbData raw frame to store
 * @param width width of frame
 * @param height height of frame
 */
void storeImageHelper(char *filename, char *rgbData, int width, int height);
/**
 * Opens web camera using v4l
 * @param dev_name camera to use using linux /dev/video notation
 * @param width frame width to use in config
 * @param height frame height to use in config
 **/
int cameraInit(char *dev_name, int width, int height);
/**
 * Grabs the next frame using the opened camera
 * @param dest memory space to store the next frame
 **/
int grabFrame(char *dest);
/**
 * closes the opened conection
 **/
void v4l2_close();
/**
 * obtains the time passed between to timevals
 **/
double timer_difference(struct timeval t2, struct timeval t1);

#endif