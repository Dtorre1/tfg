#include "webcam_capture.hpp"
#include "bmp.h"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

int main(int argc, char **argv)
{
    if (argc < 5)
    {
        fprintf(stderr, "Error, USAGE: main /dev/videoN <width> <height> <seconds>\n");
        exit(1);
    }
    int width = atoi(argv[2]);
    int height = atoi(argv[3]);
    int numFrames = atoi(argv[4]) * 30;
    struct timeval timerStart, timerEnd;
    double frameTime = 0.0;
    char *frame = new char[width * height * 3];
    cameraInit(argv[1], width, height);
    sleep(5);
    for (auto i = 0; i < numFrames; i++)
    {
        gettimeofday(&timerStart, NULL);
        grabFrame(frame);
        gettimeofday(&timerEnd, NULL);
        frameTime += timer_difference(timerEnd, timerStart);
    }
    double avg = frameTime / numFrames;
    double framerate = 1 / avg;
    // printf("average_frame_time:total_time:num_frames:framerate\n");
    printf("%dx%d:%f:%f:%d:%f:v4l2\n", width, height, avg, frameTime, numFrames, framerate);
    v4l2_close();
}
