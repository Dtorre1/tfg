int function(cv::Mat &src, cv::Mat &dst, cv::Mat )
{
    const int BLOCK_SZ = 32;
    short XY[BLOCK_SZ * BLOCK_SZ * 2], A[BLOCK_SZ * BLOCK_SZ];
    int x, y, y1, width = dst.cols, height = dst.rows;
    int bh0 = std::min(BLOCK_SZ / 2, height);
    int bw0 = std::min(BLOCK_SZ * BLOCK_SZ / bh0, width);
    bh0 = std::min(BLOCK_SZ * BLOCK_SZ / bw0, height);
    for (y = range.start; y < range.end; y += bh0)
    {
        for (x = 0; x < width; x += bw0)
        {
            int bw = std::min(bw0, width - x);
            int bh = std::min(bh0, range.end - y); // height

            Mat _XY(bh, bw, CV_16SC2, XY), matA;
            Mat dpart(dst, Rect(x, y, bw, bh));

            for (y1 = 0; y1 < bh; y1++)
            {
                short *xy = XY + y1 * bw * 2;
                double X0 = M[0] * x + M[1] * (y + y1) + M[2];
                double Y0 = M[3] * x + M[4] * (y + y1) + M[5];
                double W0 = M[6] * x + M[7] * (y + y1) + M[8];

                for (int x1 = 0; x1 < bw; x1++)
                {
                    double W = W0 + M[6] * x1;
                    W = W ? 1. / W : 0;
                    double fX = std::max((double)INT_MIN, std::min((double)INT_MAX, (X0 + M[0] * x1) * W));
                    double fY = std::max((double)INT_MIN, std::min((double)INT_MAX, (Y0 + M[3] * x1) * W));
                    int X = saturate_cast<int>(fX);
                    int Y = saturate_cast<int>(fY);

                    xy[x1 * 2] = saturate_cast<short>(X);
                    xy[x1 * 2 + 1] = saturate_cast<short>(Y);
                }
            }
            remap(src, dpart, _XY, Mat(), interpolation, borderType, borderValue);
        }
    }
}
