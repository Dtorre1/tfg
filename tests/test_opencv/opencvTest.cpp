#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <sys/time.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <thread>
#include <math.h>

double timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
    return ((double)(t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec) / 1000000.0;
}

// void showImageAndFPS(double timePassed, cv::Mat cvFrame)
// {
//     int fps = floor(1 / timePassed);
//     cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
//     char text[8];
//     char text2[30];
//     sprintf(text,"FPS:%d", fps);
//     cv::putText(cvFrame, text, cv::Point(0,30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
//     cv::imshow("Display window", cvFrame);
//     cv::waitKey(10);
// }

int main(int argc, char **argv)
{
    if (argc < 5)
    {
        fprintf(stderr, "Error, USAGE: main <numWebcam> <width> <height> <seconds>\n");
        exit(1);
    }
    int width = atoi(argv[2]);
    int height = atoi(argv[3]);
    int numFrames = atoi(argv[4]) * 30;
    struct timeval displayStart, displayEnd;
    double frameTime;
    cv::VideoCapture inputVideo;
    if (!inputVideo.open(atoi(argv[1])))
    {
        printf("Error\n");
        exit(1);
    }
    inputVideo.set(cv::CAP_PROP_FRAME_WIDTH, width);
    inputVideo.set(cv::CAP_PROP_FRAME_HEIGHT, height);
    for (auto i = 0; i < numFrames; i++)
    {
        gettimeofday(&displayStart, NULL);
        cv::Mat frame;
        inputVideo >> frame;
        gettimeofday(&displayEnd, NULL);
        frameTime += timer_difference(displayEnd, displayStart);
        // showImageAndFPS(timer_difference(displayEnd,displayStart), frame);
    }
    double avg = frameTime / numFrames;
    double framerate = 1 / avg;
    // printf("widthxheight:average_frame_time:total_time:num_frames:framerate\n");
    printf("%dx%d:%f:%f:%d:%f:opencv\n", width, height, avg, frameTime, numFrames, framerate);
}
