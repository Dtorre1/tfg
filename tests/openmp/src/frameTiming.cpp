#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

double timer_difference(struct timeval t2, struct timeval t1);

int main(int argc, char **argv)
{

    struct timeval timer_start, timer_end;
    double frame_time_avg, write_time_avg;

    cv::VideoCapture camera(0);
    if (argc < 5)
    {
        fprintf(stderr, "Usage: recordVideo <num_frames> <buffer_size in frames> <width> <height>\n");
    }
    int num_frames = atoi(argv[1]);

    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }
    int buffer_size = atoi(argv[2]);
    int frame_width = atoi(argv[3]);
    int frame_height = atoi(argv[4]);
    camera.set(cv::CAP_PROP_FRAME_WIDTH, frame_width);
    camera.set(cv::CAP_PROP_FRAME_HEIGHT, frame_height);
    camera.set(cv::CAP_PROP_CONVERT_RGB, false);
    //camera.set(cv::CAP_PROP_MODE,cv::CAP_PROP_MODE_YUYV);
    camera.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('Y','U','Y','V'));
    cv::Mat frame[buffer_size]; //will contain the frame
    cv::VideoWriter video("outcpp_blurred.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 30, cv::Size(frame_width, frame_height));

    int num_buffers = num_frames / buffer_size;
    for (auto j = 0; j < num_buffers; j++)
    {
        for (auto i = 0; i < buffer_size; i++)
        {
            gettimeofday(&timer_start, NULL);
            camera >> frame[i]; //get the frame
            gettimeofday(&timer_end, NULL);
            frame_time_avg += timer_difference(timer_end, timer_start);
        }
        for (auto i = 0; i < buffer_size; i++)
        {
            gettimeofday(&timer_start, NULL);
            video.write(frame[i]);
            gettimeofday(&timer_end, NULL);
            write_time_avg += timer_difference(timer_end, timer_start);
        }
    }

    camera.release();
    video.release();
    frame_time_avg = frame_time_avg / num_frames;
    write_time_avg = write_time_avg / num_frames;
    printf("%f:%f\n", frame_time_avg, write_time_avg);
    return 0;
}
double timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
    return ((double)(t2.tv_sec - t1.tv_sec) * 1000) + (t2.tv_usec - t1.tv_usec) / 1000.0;
}