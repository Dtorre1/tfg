#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

long timer_difference(struct timeval t2, struct timeval t1);

int main(int argc, char **argv)
{
    struct timeval timer_start, timer_end;
    double record_time, ini_time;
    gettimeofday(&timer_start, NULL);
    cv::VideoCapture camera(0);
    if (argc < 5)
    {
        fprintf(stderr, "Usage: recordVideo <num_frames> <buffer_size in frames> <width> <height>\n");
    }
    int num_frames = atoi(argv[1]);

    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }
    int buffer_size = atoi(argv[2]);
    int frame_width = atoi(argv[3]);
    int frame_height = atoi(argv[4]);
    camera.set(cv::CAP_PROP_FRAME_WIDTH, frame_width);
    camera.set(cv::CAP_PROP_FRAME_HEIGHT, frame_height);
    cv::Mat frame[buffer_size]; //will contain the frame
    gettimeofday(&timer_end, NULL);
    ini_time = timer_difference(timer_end, timer_start) / 1000000.0;

    gettimeofday(&timer_start, NULL);
    
    int num_buffers = num_frames / buffer_size;
    for (auto j = 0; j < num_buffers; j++)
    {
        for (auto i = 0; i < buffer_size; i++)
        {
            camera >> frame[i]; //get the frame
        }
        //do nothing
    }
    gettimeofday(&timer_end, NULL);
    record_time = timer_difference(timer_end, timer_start) / 1000000.0;

    camera.release();
    //width:height:type:strategy:record_time:frames:action:ini_time
    printf("%d:%d:%d:no-omp:buffer-%d:%f:record:%f\n", frame_width, frame_height, num_frames, buffer_size, record_time, ini_time);
    return 0;
}
long timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
    return (t2.tv_sec - t1.tv_sec) * 1000000 + t2.tv_usec - t1.tv_usec;
}