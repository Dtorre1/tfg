#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

void main()
{
    double grabMode, frameFormat, settings;
    bool ok = false;
    // Backed-specific value indicating the current capture mode.
    grabMode = cap.get(CV_CAP_PROP_MODE);

    int i = -100;
    while (ok == false && i < 10)
    {
        if (i != 0)
        {
            ok = cap.set(CV_CAP_PROP_MODE, grabMode + i);
            if(ok)
            {
                printf("Grab mode %d works\n",grabMode);
            }
        }
        i++;
    }
}
