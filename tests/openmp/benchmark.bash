#!/bin/bash
BENCHS=(recordVideo saveVideo)
declare -a sizes=("320 240" "640 480" "160 120")
#PERF="sudo /usr/bin/perf_5.2" #raspberry
#PERF="sudo perf" #laptop
#PERF_OPTIONS="stat -d -x ."
make all
for i in ${BENCHS[*]};
do
	for size in "${sizes[@]}";
	do
   		for j in {1..12};
    		do
    			$PERF $PERF_OPTIONS ./build/$i 200 20 $size >> pi.txt
			sleep 2
   		done
	done
done
