#!/bin/bash
cd test_Fps_V4l2
make
echo "widthxheight:average_frame_time:total_time:num_frames:framerate:lib\n" >> results.txt
for j in 5 10 15;
do
    for i in {1..12};
    do
        ./fps_test /dev/video0 640 480 $j >> results.txt
    done
done
for j in 5 10 15;
do
    for i in {1..12};
    do
        ./fps_test /dev/video0 1280 720 $j >> results.txt
    done
done
cd ..
cd test_opencv
make
for j in 5 10 15;
do
    for i in {1..12};
    do
        ./ocv 0 640 480 $j >> results.txt
    done
done
for j in 5 10 15;
do
    for i in {1..12};
    do
        ./ocv 0 1280 720 $j >> results.txt
    done
done