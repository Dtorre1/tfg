__kernel void
prueba(__global char* src, __global float* filter, __global char* dst, int rows, int cols, int filterWidth)
{
  int index = get_global_id(0);
  dst[3*index] = src[3*index];
  dst[3*index+1] = src[3*index+1];
  dst[3*index+2] = src[3*index+2];
}