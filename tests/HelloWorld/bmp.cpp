#include "bmp.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
pixel *cargarImagen(char *filename, bmpInfoHeader *infoHeader, bmpFileHeader *fileHeader)
{
    FILE *file;

    unsigned char *img;
    uint16_t type;
    pixel *datos;

    file = fopen(filename, "r");
    if (file == NULL)
    {
        return NULL;
    }

    fread(&type, sizeof(uint16_t), 1, file); //dos primeros bytes
    if (type != FORMATO_IMAGEN)
    {
        fclose(file);
        return NULL;
    }

    fread(fileHeader, sizeof(bmpFileHeader), 1, file);
    fread(infoHeader, sizeof(bmpInfoHeader), 1, file);

    img = (unsigned char *)malloc(infoHeader->imgsize);
    datos = (pixel *)malloc(infoHeader->imgsize);

    fseek(file, fileHeader->offset, SEEK_SET);
    fread(img, infoHeader->imgsize, 1, file);

    int i, j;
    for (i = 0; i < infoHeader->height; i++)
    {
        for (j = 0; j < infoHeader->width; j++)
        {
            //pasamos los datos a una estructura que nos permite tenerlo todo más ordenado
            memcpy(&datos[i * infoHeader->width + j], &img[3 * (j + i * infoHeader->width)], sizeof(pixel));
        }
    }
    free(img);
    fclose(file);
    return datos;
}

int guardarImagen(char *filename, bmpInfoHeader *infoHeader, bmpFileHeader *fileHeader, pixel *data)
{
    FILE *file;
    unsigned char *img;
    int check;

    file = fopen(filename, "w");
    if (file == NULL)
    {
        return -1;
    }

    uint16_t type = FORMATO_IMAGEN;
    check = fwrite(&type, sizeof(uint16_t), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar el formato de imagen\n");
        fclose(file);
        return -1;
    }

    check = fwrite(fileHeader, sizeof(bmpFileHeader), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar fileHeader\n");
        fclose(file);
        return -1;
    }
    check = fwrite(infoHeader, sizeof(bmpInfoHeader), 1, file);
    if (check != 1) //si no se ha escrito el elemento
    {
        fprintf(stderr, "Error al guardar infoHeader\n");
        fclose(file);
        return -1;
    }

    img = (unsigned char *)malloc(infoHeader->imgsize);

    int i, j;
    for (i = 0; i < infoHeader->height; i++)
    {
        for (j = 0; j < infoHeader->width; j++)
        {
            memcpy(&img[3 * (j + i * infoHeader->width)], &data[i * infoHeader->width + j], sizeof(pixel));
        }
    }
    fwrite(img, infoHeader->imgsize, 1, file);
    free(img);
    free(data);
    fclose(file);
    return 0;
}