#include "bmp.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>

void GaussianBlurOnCPU(pixel *dst, pixel *src, int rows, int cols, float *filter, int filterWidth)
{
    int row;
    int col;
    int i, j;

    const int mitad = filterWidth / 2;
    const int width = cols - 1;
    const int height = rows - 1;
    for (row = 0; row < rows; row++)
    {
        for (col = 0; col < cols; col++)
        {
            float blur_x = 0;
            float blur_y = 0;
            float blur_z = 0;
            for (i = -mitad; i <= mitad; ++i)
            {
                for (j = -mitad; j <= mitad; ++j)
                {
                    // Clamp filter to the image border
                    int h = std::min(std::max(row + i, 0), height);
                    int w = std::min(std::max(col + j, 0), width);
                    int idx = w + cols * h;                          // current pixel index
                    int idf = (i + mitad) * filterWidth + j + mitad; //current filter index
                    float x = (float)(src[idx].rojo);
                    float y = (float)(src[idx].verde);
                    float z = (float)(src[idx].azul);
                    float weight = filter[idf];
                    blur_x += x * weight;
                    blur_y += y * weight;
                    blur_z += z * weight;
                }
            }
            dst[row * cols + col].rojo = (unsigned char)(blur_x);
            dst[row * cols + col].verde = (unsigned char)(blur_y);
            dst[row * cols + col].azul = (unsigned char)(blur_z);
        }
    }
}

// void GaussianBlurOnGPU(pixel *modifiedImage, pixel *img, int rows, int cols, float *filter, int filterWidth)
// {

//     int img_size = rows * cols * sizeof(pixel);
//     int filter_size = filterWidth * filterWidth * sizeof(float);
//     printf("img_size %d\n", img_size);
//     printf("filter_size %d\n", filter_size);
//     int block_size = 256;
//     //int num_blocks = ceil((float)rows * cols / 256);

//     ocl_kernel gaussianBlur;
//     gaussianBlur.filename = "gauss.cl";
//     gaussianBlur.funcname = "gauss";
//     printf("%s\n", gaussianBlur.filename);
//     gaussianBlur.platformId = 1;
//     gaussianBlur.deviceId = 0;

//     gaussianBlur.input_num_memObjects = 2;
//     gaussianBlur.input_memObjects_sizes = (unsigned int *)malloc(2 * sizeof(unsigned int));
//     gaussianBlur.input_memObjects_sizes[0] = img_size;
//     gaussianBlur.input_memObjects_sizes[1] = filter_size;
//     gaussianBlur.input_memObjects = (cl_mem *)malloc(2 * sizeof(cl_mem));

//     gaussianBlur.output_num_memObjects = 1;
//     gaussianBlur.output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
//     gaussianBlur.output_memObjects_sizes[0] = rows * cols * sizeof(pixel);
//     gaussianBlur.output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

//     gaussianBlur.numArgs = 6;
//     gaussianBlur.argcounter = 0;
//     gaussianBlur.args = (void **)malloc(6 * sizeof(void *));
//     gaussianBlur.arg_sizes = (unsigned int *)malloc(6 * sizeof(void *));
    
//     cargarKernel(&gaussianBlur);

//     gaussianBlur.args[gaussianBlur.argcounter] = &rows;
//     gaussianBlur.arg_sizes[gaussianBlur.argcounter] = sizeof(rows);
//     gaussianBlur.argcounter++;

//     gaussianBlur.args[gaussianBlur.argcounter] = &cols;
//     gaussianBlur.arg_sizes[gaussianBlur.argcounter] = sizeof(cols);
//     gaussianBlur.argcounter++;

//     gaussianBlur.args[gaussianBlur.argcounter] = &filterWidth;
//     gaussianBlur.arg_sizes[gaussianBlur.argcounter] = sizeof(filterWidth);

//     cargarArgsKernel(&gaussianBlur);
//     cargarDatosEntrada((char *)img, img_size, 0, &gaussianBlur);
//     cargarDatosEntrada((char *)filter, filter_size, 1, &gaussianBlur);
//     size_t workforce[1] = {rows * cols};
//     ejecutarKernel(&gaussianBlur, workforce);
//     obtenerDatosSalida((char *)modifiedImage, img_size, 0, &gaussianBlur);
//     Cleanup(&gaussianBlur);
// }

float *createFilter(int width)
{
    const float sigma = 2.f; // Standard deviation of the Gaussian distribution.

    const int half = width / 2;
    float sum = 0.f;

    // Create convolution matrix
    float *res = (float *)malloc(width * width * sizeof(float));

    // Calculate filter sum first
    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            // e (natural logarithm base) to the power x, where x is what's in the brackets
            float weight = expf((float)(c * c + r * r) / (2.f * sigma * sigma));
            int idx = (r + half) * width + c + half;

            res[idx] = weight;
            sum += weight;
        }
    }

    // Normalize weight: sum of weights must equal 1
    float normal = 1.f / sum;

    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            int idx = (r + half) * width + c + half;

            res[idx] *= normal;
        }
    }
    return res;
}

int main(int argc, char **argv)
{
    char *imagePath;
    char *outputPath;

    int height, width;
    pixel *originalImage, *blurredImage;

    int filterWidth = 11;
    float *filter = createFilter(filterWidth);

    bmpInfoHeader infoHeader;
    bmpFileHeader fileHeader;

    if (argc > 2)
    {
        imagePath = argv[1];
        outputPath = argv[2];
    }
    else
    {
        printf("Please provide input and output image files as arguments to this application.");
        exit(1);
    }

    //Read the image
    originalImage = cargarImagen(imagePath, &infoHeader, &fileHeader);
    height = infoHeader.height;
    width = infoHeader.width;
    blurredImage = (pixel *)malloc(height * width * sizeof(pixel));

    GaussianBlurOnCPU(blurredImage, originalImage, height, width, filter, filterWidth);
    guardarImagen(outputPath, &infoHeader, &fileHeader, blurredImage);
    free(originalImage);
    printf("Done!\n");
    return 0;
}
