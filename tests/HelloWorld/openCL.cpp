#include "openCL.h"
#include <CL/cl.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CL_CHECK(_expr)                                                          \
    do                                                                           \
    {                                                                            \
        cl_int _err = _expr;                                                     \
        if (_err == CL_SUCCESS)                                                  \
            break;                                                               \
        fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
        abort();                                                                 \
    } while (0)

#define CL_CHECK_ERR(_expr)                                                          \
    ({                                                                               \
        cl_int _err = CL_INVALID_VALUE;                                              \
        typeof(_expr) _ret = _expr;                                                  \
        if (_err != CL_SUCCESS)                                                      \
        {                                                                            \
            fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
            abort();                                                                 \
        }                                                                            \
        _ret;                                                                        \
    })

int CreateProgram(ocl_kernel *ocl_program)
{
    cl_int errNum;

    std::ifstream kernelFile(ocl_program->filename, std::ios::in);
    if (!kernelFile.is_open())
    {
        std::cerr << "Failed to open file for reading: " << ocl_program->filename << std::endl;
        return 0;
    }

    std::ostringstream oss;
    oss << kernelFile.rdbuf();

    std::string srcStdStr = oss.str();
    const char *srcStr = srcStdStr.c_str();
    cl_int err;
    ocl_program->program = clCreateProgramWithSource(ocl_program->context, 1, (const char **)&srcStr, NULL, &err);
    if (err != CL_SUCCESS)
    {
        printf("error %d in createProgram\n", err);
    }
    if (ocl_program->program == NULL)
    {
        std::cerr << "Failed to create CL program from source." << std::endl;
        return 0;
    }

    errNum = clBuildProgram(ocl_program->program, 0, NULL, NULL, NULL, NULL);
    if (errNum != CL_SUCCESS)
    {
        printf("Error encountered when building the program: %d\n", errNum);
        // Determine the reason for the error
        char buildLog[16384];
        clGetProgramBuildInfo(
            ocl_program->program,  ocl_program->device_id, CL_PROGRAM_BUILD_LOG, sizeof(buildLog), buildLog, NULL);

        std::cerr << buildLog;
        clReleaseProgram(ocl_program->program);
        return 0;
    }
    return 1;
}

///
//  Cleanup any created OpenCL resources
//
void Cleanup(ocl_kernel *programa)
{
    for (int i = 0; i < programa->input_num_memObjects; i++)
    {
        if (programa->input_memObjects[i] != 0)
            clReleaseMemObject(programa->input_memObjects[i]);
    }
    for (int i = 0; i < programa->output_num_memObjects; i++)
    {
        if (programa->output_memObjects[i] != 0)
            clReleaseMemObject(programa->output_memObjects[i]);
    }
    if (programa->queue != 0)
        clReleaseCommandQueue(programa->queue);

    if (programa->kernel != 0)
        clReleaseKernel(programa->kernel);

    if (programa->program != 0)
        clReleaseProgram(programa->program);

    if (programa->context != 0)
        clReleaseContext(programa->context);
}

void cargarKernel(ocl_kernel *ocl_program)
{
    cl_platform_id platforms[100];
    cl_uint platforms_n = 0;
    cl_device_id devices[100];
    cl_uint devices_n = 0;
    const cl_queue_properties qproperties[] = {CL_QUEUE_PROPERTIES, CL_QUEUE_PROFILING_ENABLE, 0};
    int buff_size = 10240;
    char buffer[buff_size];
    int i;

    CL_CHECK(clGetPlatformIDs(100, platforms, &platforms_n));

    printf("=== %d OpenCL platform(s) found: ===\n", platforms_n);

    for (unsigned int i = 0; i < platforms_n; i++)
    {
        printf("  -- %d --\n", i);
        CL_CHECK(clGetPlatformInfo(
            platforms[i], CL_PLATFORM_NAME, buff_size, buffer, NULL));
        printf("  NAME = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(
            platforms[i], CL_PLATFORM_VENDOR, buff_size, buffer, NULL));
        printf("  VENDOR = %s\n", buffer);
    }

    CL_CHECK(clGetDeviceIDs(platforms[ocl_program->platformId], CL_DEVICE_TYPE_ALL, 100, devices, &devices_n));

    printf("=== %d OpenCL device(s) found on platform:\n", platforms_n);
    for (unsigned int i = 0; i < devices_n; i++)
    {
        cl_uint buf_uint;
        cl_ulong buf_ulong;
        size_t wi_size[3];
        printf("  -- %d --\n", i);
        CL_CHECK(clGetDeviceInfo(
            devices[i], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL));
        printf("  DEVICE_NAME = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(
            devices[i], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL));
        printf("  DEVICE_VENDOR = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(
            devices[i], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL));
    }
    printf("Creating context...\n");

    ocl_program->device_id = devices[ocl_program->deviceId];
    ocl_program->context = CL_CHECK_ERR(
        clCreateContext(NULL, 1, &ocl_program->device_id, &pfn_notify, NULL, &_err));

    printf("Creating command queue...\n");

    ocl_program->queue = CL_CHECK_ERR(clCreateCommandQueueWithProperties(
        ocl_program->context, ocl_program->device_id, qproperties, &_err));

    printf("Creating program...\n");
    ocl_program->program = NULL;
    CreateProgram(ocl_program);
    if (ocl_program->program == NULL)
    {
        Cleanup(ocl_program);
        exit(1);
    }

    printf("attempting to create input buffers\n");
    fflush(stdout);
    for (i = 0; i < ocl_program->input_num_memObjects; i++)
    {

        ocl_program->input_memObjects[i] = CL_CHECK_ERR(clCreateBuffer(
            ocl_program->context,
            CL_MEM_READ_ONLY,
            ocl_program->input_memObjects_sizes[i],
            NULL, &_err));

        ocl_program->args[ocl_program->argcounter] = &ocl_program->input_memObjects[i];
        ocl_program->arg_sizes[ocl_program->argcounter] = sizeof(ocl_program->input_memObjects[i]);
        ocl_program->argcounter++;
    }

    printf("attempting to create output buffers\n");
    fflush(stdout);
    for (i = 0; i < ocl_program->output_num_memObjects; i++)
    {
        ocl_program->output_memObjects[i] = CL_CHECK_ERR(clCreateBuffer(
            ocl_program->context,
            CL_MEM_WRITE_ONLY,
            ocl_program->output_memObjects_sizes[i],
            NULL, &_err));
        ocl_program->args[ocl_program->argcounter++] = &ocl_program->output_memObjects[i];
        ocl_program->arg_sizes[ocl_program->argcounter - 1] = sizeof(ocl_program->output_memObjects[i]);
    }

    printf("attempting to create kernel\n");
    fflush(stdout);
    ocl_program->kernel = CL_CHECK_ERR(clCreateKernel(ocl_program->program, ocl_program->funcname, &_err));
}

void cargarArgsKernel(ocl_kernel *ocl_program)
{
    int i;
    printf("setting up kernel args cl_mem: \n");
    for (i = 0; i < ocl_program->numArgs; i++)
    {
        CL_CHECK(clSetKernelArg(ocl_program->kernel, i, ocl_program->arg_sizes[i], ocl_program->args[i]));
    }
}

void cargarDatosEntrada(char *data, int size, int numBuffer, ocl_kernel *ocl_program)
{
    printf("attempting to enqueue write buffers\n");
    fflush(stdout);

    CL_CHECK(clEnqueueWriteBuffer(
        ocl_program->queue,
        ocl_program->input_memObjects[numBuffer],
        CL_TRUE,
        0, size, data, 0, NULL, NULL));
}

void ejecutarKernel(ocl_kernel *ocl_program, const size_t *worksize)
{

    cl_event kernel_completion;
    printf("attempting to enqueue kernel\n");
    fflush(stdout);
    CL_CHECK(clEnqueueNDRangeKernel(ocl_program->queue,
                                    ocl_program->kernel,
                                    1,
                                    NULL,
                                    worksize,
                                    NULL,
                                    0,
                                    NULL,
                                    &kernel_completion));
    printf("Enqueue'd kernel\n");
    fflush(stdout);
    CL_CHECK(clWaitForEvents(1, &kernel_completion));
    printf("kernel executed\n");
}

void obtenerDatosSalida(char *data, int size, int numBuffer, ocl_kernel *ocl_program)
{
    printf("attempting to read results\n");
    CL_CHECK(clEnqueueReadBuffer(
        ocl_program->queue,
        ocl_program->output_memObjects[numBuffer],
        CL_TRUE,
        0, size, data, 0, NULL, NULL));
}

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
    fprintf(stderr, "OpenCL Error (via pfn_notify): %s\n", errinfo);
}