#include <CL/cl.h>
#ifndef OPENCL_H
#define OPENCL_H
typedef struct ocl_kernel
{
  //filled by the user
  char *filename; //name of the kernel
  char *funcname;
  int platformId; //platform to be used
  int deviceId; //device number to be used

  cl_device_id device_id;
  unsigned int input_num_memObjects; //number of input buffers to be used
  size_t *input_memObjects_sizes; //array of sizes of the buffers

  unsigned int output_num_memObjects;
  size_t *output_memObjects_sizes;

  unsigned int numArgs; //number of args the kernel has

  void **args; //array of arg pointers, allocated by the user, filled by both
  //kernel arguments must be ordered by this criteria: input_buffers, output buffers, args passed by value
  unsigned int *arg_sizes;//size of each arg
  int argcounter; //counter to use **args correctly

  //filled by the library
  cl_mem *input_memObjects;
  cl_mem *output_memObjects;
  
  cl_context context;
  cl_command_queue queue;
  cl_kernel kernel;
  cl_program program;

} ocl_kernel;

void CreateProgram(cl_context context, cl_device_id device, const char *fileName);
void Cleanup(ocl_kernel *programa);
void cargarKernel(ocl_kernel *ocl_program);
void cargarArgsKernel(ocl_kernel *ocl_program);
void cargarDatosEntrada(char *data, int size, int numBuffer, ocl_kernel *ocl_program);
void ejecutarKernel(ocl_kernel *ocl_program, const size_t *worksize);
void obtenerDatosSalida(char *data, int size, int numBuffer, ocl_kernel *ocl_program);

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data);
#endif