
typedef struct T
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
} T;

typedef struct T4
{
    T x;
    T y;
    T z;
    T w;
} T4;

#define loadpix(addr)  *(__global const T*)(addr)
#define storepix(val, addr)  *(__global T*)(addr) = val
#define pixsize (int)sizeof(T)
#define TSIZE (int)sizeof(T)
#define OP(a,b) {    mid=a; a=minT(a,b); b=maxT(mid,b);}



__kernel void median(__global const uchar * srcptr, __global uchar * dstptr, int src_step, int src_offset,
                             int dst_step, int dst_offset, int rows, int cols, int blocksizeX)
{
    __local T data[4][3];

    // int x = get_local_id(0);
    // int y = get_local_id(1);
    // int gx = get_global_id(0);
    // int gy = get_global_id(1);
    int index = get_global_id(0);
    int lindex = get_local_id(0);
    int gx = index / rows;
    int gy = index % rows;
    int x = lindex / blocksizeX;
    int y = lindex % blocksizeX;

    int dx = gx - x - 1;
    int dy = gy - y - 1;

    int id = min(mad24(x, 16, y), 9*18-1);

    int dr = id / 18;
    int dc = id % 18;

    int c = clamp(dx + dc, 0, cols - 1);

    int r = clamp(dy + dr, 0, rows - 1);
    int index1 = mad24(r, src_step, mad24(c, TSIZE, src_offset));
    r = clamp(dy + dr + 9, 0, rows - 1);
    int index9 = mad24(r, src_step, mad24(c, TSIZE, src_offset));

    data[dr][dc] = loadpix(srcptr + index1);
    data[dr+9][dc] = loadpix(srcptr + index9);
    barrier(CLK_LOCAL_MEM_FENCE);

    T p0 = data[y][x], p1 = data[y][(x+1)], p2 = data[y][(x+2)];
    T p3 = data[y+1][x], p4 = data[y+1][(x+1)], p5 = data[y+1][(x+2)];
    T p6 = data[y+2][x], p7 = data[y+2][(x+1)], p8 = data[y+2][(x+2)];
    T mid;

    T minT(T a, T b);//declarations
    T maxT(T a, T b);

    OP(p1, p2); OP(p4, p5); OP(p7, p8); OP(p0, p1);
    OP(p3, p4); OP(p6, p7); OP(p1, p2); OP(p4, p5);
    OP(p7, p8); OP(p0, p3); OP(p5, p8); OP(p4, p7);
    OP(p3, p6); OP(p1, p4); OP(p2, p5); OP(p4, p7);
    OP(p4, p2); OP(p6, p4); OP(p4, p2);

    int dst_index = mad24( gy, dst_step, mad24(gx, TSIZE, dst_offset));

    if (gy < rows && gx < cols)
        storepix(p4, dstptr + dst_index);
}

T minT(T a, T b)
{
    unsigned char x;
    unsigned char y;
    unsigned char z;
    x = a.x - b.x;
    y = a.y - b.y;
    z = a.z - b.z;
    int result = 0;
    result += x < 0 ? 1 : 0;
    result += y < 0 ? 1 : 0;
    result += z < 0 ? 1 : 0;
    return (result >= 2 ? a : b);
}
T maxT(T a, T b)
{
    unsigned char x;
    unsigned char y;
    unsigned char z;
    x = a.x - b.x;
    y = a.y - b.y;
    z = a.z - b.z;
    int result = 0;
    result += x < 0 ? 1 : 0;
    result += y < 0 ? 1 : 0;
    result += z < 0 ? 1 : 0;
    return (result >= 2 ? b : a);
}