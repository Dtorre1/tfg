#include "openCL.h"
#include <CL/cl.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SILENT 1

cl_platform_id platforms[100];
cl_uint platforms_n = 0;
cl_device_id devices[100];
cl_uint devices_n = 0;
int buff_size = 10240;
char buffer[10240];

#define CL_CHECK(_expr)                                                          \
    do                                                                           \
    {                                                                            \
        cl_int _err = _expr;                                                     \
        if (_err == CL_SUCCESS)                                                  \
            break;                                                               \
        fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
        abort();                                                                 \
    } while (0)

#define CL_CHECK_ERR(_expr)                                                          \
    ({                                                                               \
        cl_int _err = CL_INVALID_VALUE;                                              \
        typeof(_expr) _ret = _expr;                                                  \
        if (_err != CL_SUCCESS)                                                      \
        {                                                                            \
            fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err); \
            abort();                                                                 \
        }                                                                            \
        _ret;                                                                        \
    })

int createProgram(ocl_kernel *ocl_program)
{
    cl_int errNum;

    std::ifstream kernelFile(ocl_program->filename, std::ios::in);
    if (!kernelFile.is_open())
    {
        std::cerr << "Failed to open file for reading: " << ocl_program->filename << std::endl;
        return 0;
    }

    std::ostringstream oss;
    oss << kernelFile.rdbuf();

    std::string srcStdStr = oss.str();
    const char *srcStr = srcStdStr.c_str();
    cl_int err;
    ocl_program->program = clCreateProgramWithSource(ocl_program->context, 1, (const char **)&srcStr, NULL, &err);
    if (err != CL_SUCCESS)
    {
        printf("error %d in createProgram\n", err);
    }
    if (ocl_program->program == NULL)
    {
        std::cerr << "Failed to create CL program from source." << std::endl;
        return 0;
    }

    errNum = clBuildProgram(ocl_program->program, 0, NULL, NULL, NULL, NULL);
    if (errNum != CL_SUCCESS)
    {
        printf("Error encountered when building the program: %d\n", errNum);
        // Determine the reason for the error
        char buildLog[60000];
        clGetProgramBuildInfo(
            ocl_program->program, ocl_program->device_id, CL_PROGRAM_BUILD_LOG, sizeof(buildLog), buildLog, NULL);

        std::cerr << buildLog;
        clReleaseProgram(ocl_program->program);
        return 0;
    }
    return 1;
}

///
//  Cleanup any created OpenCL resources
//
void cleanup(ocl_kernel *programa)
{
    for (int i = 0; i < programa->input_num_memObjects; i++)
    {
        if (programa->input_memObjects[i] != 0)
            clReleaseMemObject(programa->input_memObjects[i]);
    }
    for (int i = 0; i < programa->output_num_memObjects; i++)
    {
        if (programa->output_memObjects[i] != 0)
            clReleaseMemObject(programa->output_memObjects[i]);
    }
    if (programa->queue != 0)
        clReleaseCommandQueue(programa->queue);

    if (programa->kernel != 0)
        clReleaseKernel(programa->kernel);

    if (programa->program != 0)
        clReleaseProgram(programa->program);

    if (programa->context != 0)
        clReleaseContext(programa->context);
}

void loadKernel(ocl_kernel *ocl_program)
{
  
    // const cl_queue_properties qproperties[] = {CL_QUEUE_PROPERTIES, CL_QUEUE_PROFILING_ENABLE, 0};
    cl_command_queue_properties qproperties = CL_QUEUE_PROFILING_ENABLE;
    CL_CHECK(clGetDeviceIDs(platforms[ocl_program->platform_num], CL_DEVICE_TYPE_ALL, 100, devices, &devices_n));
    #ifndef SILENT
        printf("Creating context...\n");
    #endif
    ocl_program->device_id = devices[ocl_program->device_num];
    ocl_program->context = CL_CHECK_ERR(
        clCreateContext(NULL, 1, &ocl_program->device_id, &pfn_notify, NULL, &_err));
    #ifndef SILENT
        printf("Creating command queue...\n");
    #endif
    // ocl_program->queue = CL_CHECK_ERR(clCreateCommandQueueWithProperties(
    //     ocl_program->context, ocl_program->device_id, qproperties, &_err));
    ocl_program->queue = CL_CHECK_ERR(clCreateCommandQueue(
        ocl_program->context, ocl_program->device_id, qproperties, &_err));
    #ifndef SILENT
        printf("Creating program...\n");
    #endif
    ocl_program->program = NULL;
    createProgram(ocl_program);
    if (ocl_program->program == NULL)
    {
        cleanup(ocl_program);
        exit(1);
    }
#ifndef SILENT
    printf("attempting to create input buffers\n");
    fflush(stdout);
#endif
    for (auto i = 0; i < ocl_program->input_num_memObjects; i++)
    {

        ocl_program->input_memObjects[i] = CL_CHECK_ERR(clCreateBuffer(
            ocl_program->context,
            CL_MEM_READ_ONLY,
            ocl_program->input_memObjects_sizes[i],
            NULL, &_err));

        ocl_program->args[ocl_program->argcounter] = &ocl_program->input_memObjects[i];
        ocl_program->arg_sizes[ocl_program->argcounter] = sizeof(ocl_program->input_memObjects[i]);
        ocl_program->argcounter++;
    }
#ifndef SILENT
    printf("attempting to create output buffers\n");
    fflush(stdout);
#endif
    for (auto i = 0; i < ocl_program->output_num_memObjects; i++)
    {
        ocl_program->output_memObjects[i] = CL_CHECK_ERR(clCreateBuffer(
            ocl_program->context,
            CL_MEM_WRITE_ONLY,
            ocl_program->output_memObjects_sizes[i],
            NULL, &_err));
        ocl_program->args[ocl_program->argcounter++] = &ocl_program->output_memObjects[i];
        ocl_program->arg_sizes[ocl_program->argcounter - 1] = sizeof(ocl_program->output_memObjects[i]);
    }
#ifndef SILENT
    printf("attempting to create kernel\n");
    fflush(stdout);
#endif
    ocl_program->kernel = CL_CHECK_ERR(clCreateKernel(ocl_program->program, ocl_program->funcname, &_err));
}

void loadKernelArgs(ocl_kernel *ocl_program)
{
    int i;
#ifndef SILENT
    printf("setting up kernel args cl_mem: \n");
#endif
    for (i = 0; i < ocl_program->num_args; i++)
    {
        CL_CHECK(clSetKernelArg(ocl_program->kernel, i, ocl_program->arg_sizes[i], ocl_program->args[i]));
    }
}

void loadInputData(char *data, int size, int numBuffer, ocl_kernel *ocl_program)
{
#ifndef SILENT
    printf("attempting to enqueue write buffers\n");
#endif
    fflush(stdout);
    CL_CHECK(clEnqueueWriteBuffer(
        ocl_program->queue,
        ocl_program->input_memObjects[numBuffer],
        CL_FALSE,
        0, size, data, 0, NULL, 
        &ocl_program->inputLoadCompletion[numBuffer]));
}



void executeKernel(ocl_kernel *ocl_program)
{

    #ifndef SILENT
        printf("attempting to enqueue kernel\n");
        fflush(stdout);
    #endif
    size_t workforce[1] = {ocl_program->worksizeGL1};
    size_t local[1] = {ocl_program->blockSize};
    CL_CHECK(clEnqueueNDRangeKernel(ocl_program->queue,
                                ocl_program->kernel,
                                1,
                                NULL,
                                workforce,
                                local,
                                ocl_program->input_num_memObjects,
                                ocl_program->inputLoadCompletion,
                                &ocl_program->kernel_completion));
    #ifndef SILENT
        printf("Enqueue'd kernel\n");
        fflush(stdout);
    #endif
}

void executeKernel2D(ocl_kernel *ocl_program)
{

    #ifndef SILENT
        printf("attempting to enqueue kernel\n");
        fflush(stdout);
    #endif
    size_t workforce[2] = {ocl_program->worksizeGL1, ocl_program->worksizeGL2};
    // size_t local[2] = {ocl_program->worksizeLC1, ocl_program->worksizeLC2};
    CL_CHECK(clEnqueueNDRangeKernel(ocl_program->queue,
                                ocl_program->kernel,
                                2,
                                NULL,
                                workforce,
                                NULL,
                                ocl_program->input_num_memObjects,
                                ocl_program->inputLoadCompletion,
                                &ocl_program->kernel_completion));
    #ifndef SILENT
        printf("Enqueue'd kernel\n");
        fflush(stdout);
    #endif
}

void waitForKernel(ocl_kernel *ocl_program)
{
    CL_CHECK(clWaitForEvents(1, &ocl_program->kernel_completion));
#ifndef SILENT
    printf("kernel executed\n");
#endif
}

void getOutputData(char *data, int size, int numBuffer, ocl_kernel *ocl_program)
{
#ifndef SILENT
    printf("attempting to read results\n");
#endif
    CL_CHECK(clEnqueueReadBuffer(
        ocl_program->queue,
        ocl_program->output_memObjects[numBuffer],
        CL_TRUE,
        0, size, data, 0, NULL, NULL));
}

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
    fprintf(stderr, "OpenCL Error (via pfn_notify): %s\n", errinfo);
}

void printMachineInfo()
{
    
    CL_CHECK(clGetPlatformIDs(100, platforms, &platforms_n));
    
    fprintf(stderr, "=== %d OpenCL platform(s) found: ===\n", platforms_n);
    for (unsigned int i = 0; i < platforms_n; i++)
    {
        fprintf(stderr, "  -- %d --\n", i);
        CL_CHECK(clGetPlatformInfo(
            platforms[i], CL_PLATFORM_NAME, buff_size, buffer, NULL));
        fprintf(stderr, "  NAME = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(
            platforms[i], CL_PLATFORM_VENDOR, buff_size, buffer, NULL));
        fprintf(stderr, "  VENDOR = %s\n", buffer);
        
        CL_CHECK(clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 100, devices, &devices_n));
        fprintf(stderr, "=== %d OpenCL device(s) found on platform: %d\n", devices_n, i);
        for (unsigned int j = 0; j < devices_n; j++)
        {
            cl_uint buf_uint;
            cl_ulong buf_ulong;
            size_t wi_size[3];
            fprintf(stderr, "  -- %d --\n", j);
            CL_CHECK(clGetDeviceInfo(
                devices[j], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL));
            fprintf(stderr, "  DEVICE_NAME = %s\n", buffer);
            CL_CHECK(clGetDeviceInfo(
                devices[j], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL));
            fprintf(stderr, "  DEVICE_VENDOR = %s\n", buffer);
            CL_CHECK(clGetDeviceInfo(
                devices[j], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL));
            fprintf(stderr, "  DEVICE_VERSION = %s\n", buffer);
        }
    }
    
    
}