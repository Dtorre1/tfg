#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include <sys/time.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <thread>
#include <math.h>
#include "webcam_capture.hpp"

void showImageAndFPS(double timePassed, cv::Mat cvFrame)
{
    int fps = floor(1 / timePassed);
    cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE); // Create a window for display.
    char text[8];
    char text2[30];
    sprintf(text, "FPS:%d", fps);
    cv::putText(cvFrame, text, cv::Point(0, 30), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
    cv::imshow("Display window", cvFrame);
    cv::waitKey(10);
}

int main(int argv, char **argc)
{
    struct timeval displayStart, displayEnd;
    double timePassed;
    int width = 640;
    int height = 480;
    char frame[width*height*3];
    cameraInit("/dev/video0",width , height);
    int fps = 200;
    for (auto i = 0; i < fps; i++)
    {
        gettimeofday(&displayStart, NULL);
        grabFrame(frame);
        cv::Mat cvFrame(height, width, CV_8UC3, frame);
        cvFrame.data = (uchar *)frame;
        gettimeofday(&displayEnd, NULL);
        showImageAndFPS(timer_difference(displayEnd, displayStart), cvFrame);
    }
}
