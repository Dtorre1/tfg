#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/core/ocl.hpp"
#include <sys/time.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <thread>
#include <math.h>
#include "openCL.h"

double timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
    return ((double)(t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec) / 1000000.0;
}

// void showImageAndFPS(double timePassed, cv::Mat cvFrame)
// {
//     int fps = floor(1 / timePassed);
//     cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
//     char text[8];
//     char text2[30];
//     sprintf(text,"FPS:%d", fps);
//     cv::putText(cvFrame, text, cv::Point(0,30),cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(118, 185, 0), 2);
//     cv::imshow("Display window", cvFrame);
//     cv::waitKey(10);
// }

void setUp(ocl_kernel *kernel, int height, int width, int step)
{
    int imgSize = width * height * sizeof(char) * 3;
    printMachineInfo();
    fprintf(stderr, "Put platform number: \n");
    scanf("%d", &kernel->platform_num);
    fprintf(stderr, "Put device number: \n");
    scanf("%d", &kernel->device_num);
    kernel->worksizeGL1 = height;
    kernel->worksizeGL2 = width;
    kernel->filename = "warpPerspective.cl";
    kernel->funcname = "warpPerspective";
    kernel->input_num_memObjects = 2;
    kernel->input_memObjects_sizes = (unsigned int *)malloc(kernel->input_num_memObjects * sizeof(unsigned int));
    kernel->input_memObjects_sizes[0] = imgSize;
    kernel->input_memObjects_sizes[1] = 9 * sizeof(double);
    kernel->input_memObjects = (cl_mem *)malloc(kernel->input_num_memObjects * sizeof(cl_mem));
    kernel->inputLoadCompletion = (cl_event *)malloc(kernel->input_num_memObjects * sizeof(cl_event));

    kernel->output_num_memObjects = 1;
    kernel->output_memObjects_sizes = (unsigned int *)malloc(1 * sizeof(unsigned int));
    kernel->output_memObjects_sizes[0] = imgSize;
    kernel->output_memObjects = (cl_mem *)malloc(1 * sizeof(cl_mem));

    kernel->num_args = 11;
    kernel->argcounter = 0;
    kernel->args = (void **)malloc(kernel->num_args * sizeof(void *));
    kernel->arg_sizes = (unsigned int *)malloc(kernel->num_args * sizeof(void *));

    loadKernel(kernel);
    int cero = 0;
    kernel->args[kernel->argcounter] = &step;
    kernel->arg_sizes[kernel->argcounter] = sizeof(step);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &cero;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &height;
    kernel->arg_sizes[kernel->argcounter] = sizeof(height);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &width;
    kernel->arg_sizes[kernel->argcounter] = sizeof(width);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &step;
    kernel->arg_sizes[kernel->argcounter] = sizeof(step);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &cero;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &height;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    kernel->args[kernel->argcounter] = &width;
    kernel->arg_sizes[kernel->argcounter] = sizeof(cero);
    kernel->argcounter++;
    loadKernelArgs(kernel);
}

void execute(ocl_kernel *kernel, cv::Mat &frame, cv::Mat &m)
{
    loadInputData((char *)frame.data, kernel->input_memObjects_sizes[0], 0, kernel);
    loadInputData((char *)m.data, kernel->input_memObjects_sizes[1], 1, kernel);
    executeKernel2D(kernel);
}

void kernelWaitAndRetrieve(ocl_kernel *kernel, void **destination)
{
    waitForKernel(kernel);
    for (auto i = 0; i < kernel->output_num_memObjects; i++)
    {
        getOutputData((char *)destination[i], kernel->output_memObjects_sizes[i], i, kernel);
    }
}

int main(int argc, char **argv)
{
    if (argc < 4)
    {
        fprintf(stderr, "Error, USAGE: main <numWebcam> <width> <height> <seconds>\n");
        exit(1);
    }
    int width = atoi(argv[2]);
    int height = atoi(argv[3]);
    struct timeval displayStart, displayEnd;
    double frameTime;
    cv::VideoCapture inputVideo;
    if (!inputVideo.open(atoi(argv[1])))
    {
        printf("Error\n");
        exit(1);
    }
    inputVideo.set(cv::CAP_PROP_FRAME_WIDTH, width);
    inputVideo.set(cv::CAP_PROP_FRAME_HEIGHT, height);

    gettimeofday(&displayStart, NULL);
    cv::Mat frame;
    cv::Mat rotated;
    std::vector<cv::Point2f> points1, points2;
    points1.push_back(cv::Point2f(10, 10));
    points1.push_back(cv::Point2f(300, 100));
    points1.push_back(cv::Point2f(10, 400));
    points1.push_back(cv::Point2f(300, 450));
    points2.push_back(cv::Point2f(10, 10));
    points2.push_back(cv::Point2f(300, 10));
    points2.push_back(cv::Point2f(10, 400));
    points2.push_back(cv::Point2f(300, 400));
    cv::Mat transform = getPerspectiveTransform(points1, points2);
    ocl_kernel kernel;
    for (auto i = 0; i < 100; i++)
        inputVideo >> frame;
    cv::imwrite("og.png", frame);
    setUp(&kernel, height, width, frame.step1());
    execute(&kernel, frame, transform);
    kernelWaitAndRetrieve(&kernel, (void **)&frame.data);
    cv::imwrite("out.png", frame);
    inputVideo.release();
}