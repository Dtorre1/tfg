#include <CL/cl.h>
#ifndef OPENCL_H
#define OPENCL_H
typedef struct ocl_kernel
{
  //filled by the user
  char *filename; //name of the kernel
  char *funcname;
  int platform_num; //platform to be used
  int device_num; //device number to be used
  cl_device_id device_id;
  unsigned int input_num_memObjects; //number of input buffers to be used
  unsigned int *input_memObjects_sizes; //array of sizes of the buffers

  unsigned int output_num_memObjects;
  unsigned int *output_memObjects_sizes;

  unsigned int num_args; //number of args the kernel has

  void **args; //array of arg pointers, allocated by the user, filled by both
  //kernel arguments must be ordered by this criteria: input_buffers, output buffers, args passed by value
  unsigned int *arg_sizes;//size of each arg
  int argcounter; //counter to use **args correctly

  //filled by the library
  cl_mem *input_memObjects;
  cl_mem *output_memObjects;

  
  cl_context context;
  cl_command_queue queue;
  cl_kernel kernel;
  cl_program program;
  cl_event kernel_completion;
  cl_event *inputLoadCompletion;

  int numBlocks;
  int blockSize;

  int worksizeGL1;
  int worksizeGL2;
  int worksizeLC1;
  int worksizeLC2;

} ocl_kernel;

/**
 * builds a program for target context and device
 * @param context
 * @param device device to use
 * @param filename filename of the kernel ex: gauss.cl
 */
void createProgram(cl_context context, cl_device_id device, const char *fileName);
/**
 * frees resources related to the kernel
 * @param programa kernel to free
 */
void cleanup(ocl_kernel *programa);
/**
 * loads the kernel on target platform
 */
void loadKernel(ocl_kernel *ocl_program);
/**
 * Loads the kernel args
 */
void loadKernelArgs(ocl_kernel *ocl_program);
/**
 * loads the input queues
 * @param data data to load
 * @param size size of the data
 * @param numBuffer input queue to use
 * @param ocl_program struct with kernel data and config
 */
void loadInputData(char *data, int size, int numBuffer, ocl_kernel *ocl_program);
/**
 * Executes the kernel
 */
void executeKernel(ocl_kernel *ocl_program);
/**
 * Retrieves output data from data queues
 * @param data data to load
 * @param size size of the data
 * @param numBuffer input queue to use
 * @param ocl_program struct with kernel data and config
 */
void getOutputData(char *data, int size, int numBuffer, ocl_kernel *ocl_program);
/**
 * Waits for kernel completion.
 */
void waitForKernel(ocl_kernel *ocl_program);
void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data);
void printMachineInfo();
void executeKernel2D(ocl_kernel *ocl_program);
#endif
