#include "webcam_capture.hpp"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "opencv2/videoio.hpp"
int main(int argc, char **argv)
{

        struct timeval timer_start, timer_end;
        double frame_time_avg = 0;
        double write_time_avg = 0;
        // int buffer_size = atoi(argv[2]);
        // int frame_width = atoi(argv[3]);
        // int frame_height = atoi(argv[4]);
        int height = 480;
        int width = 640;

        cameraInit();
        char *filename = "porfavorfunciona.avi";
        cv::VideoWriter video(filename, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 30, cv::Size(width, height));
        int iter = 50;
        char *frame = (char *)malloc(height * width * 3 * sizeof(char));
        for (auto i = 0; i < iter; i++)
        {
                gettimeofday(&timer_start, NULL);
                grabFrame(frame);
                gettimeofday(&timer_end, NULL);

                frame_time_avg += timer_difference(timer_end, timer_start);

                gettimeofday(&timer_start, NULL);

                cv::Mat cvFrame(height, width, CV_8UC3, frame);
                std::memcpy(cvFrame.data, frame, height * width * sizeof(char) * 3);
                video.write(cvFrame);

                gettimeofday(&timer_end, NULL);

                write_time_avg += timer_difference(timer_end, timer_start);
        }
        printf("Grab avg: %f\n", frame_time_avg / iter);
        printf("Write avg: %f\n", write_time_avg / iter);
        v4l2_close();
        video.release();
        return 0;
}