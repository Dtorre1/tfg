//include guard start
#ifndef BMP_H
#define BMP_H

#include <stdint.h>

typedef struct bmpFileHeader
{
  /* 2 bytes de identificación */
  uint32_t size;   /* Tamaño del archivo */
  uint16_t resv1;  /* Reservado */
  uint16_t resv2;  /* Reservado */
  uint32_t offset; /* Offset hasta hasta los datos de imagen */
} bmpFileHeader;

typedef struct bmpInfoHeader
{
  uint32_t headersize; /* Tamaño de la cabecera */
  uint32_t width;      /* Ancho */
  uint32_t height;     /* Alto */
  uint16_t planes;     /* Planos de color (Siempre 1) */
  uint16_t bpp;        /* bits por pixel */
  uint32_t compress;   /* compresión */
  uint32_t imgsize;    /* tamaño de los datos de imagen */
  uint32_t bpmx;       /* Resolución X en bits por metro */
  uint32_t bpmy;       /* Resolución Y en bits por metro */
  uint32_t colors;     /* colors used en la paleta */
  uint32_t imxtcolors; /* Colores importantes. 0 si son todos */
} bmpInfoHeader;

typedef struct pixel 
{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
} pixel;

#define IMAGE_FORMAT 0x4D42
#define BMP_BPP 24
#define BMP_COLORS 0
#define BMP_IMPORTANT_COLORS 0
#define BMP_COMPRESSION 0
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40
#define BMP_BPM 3780
#define BMP_SIZE 54
#define BMP_OFFSET 54

pixel *loadImage(char *filename, bmpInfoHeader *infoHeader, bmpFileHeader *fileHeader);
int storeImage(char *filename, bmpInfoHeader *infoHeader, bmpFileHeader *fileHeader, char *data);

#endif
//Include guard end