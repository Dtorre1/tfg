#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/videoio.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <iostream>
int main(int argc, char **argv)
{

    if (argc < 4)
    {
        std::cerr << "USAGE ./webcam <num_webcam> <num_seconds> <FPS>" << std::endl;
    }
    // open the first webcam plugged in the computer
    cv::VideoCapture camera(atoi(argv[1]));
    int fps = atoi(argv[3]);
    int num_frames = atoi(argv[2]) * fps;

    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }

    // Default resolution of the frame is obtained.The default resolution is system dependent.
    int frame_width = camera.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = camera.get(cv::CAP_PROP_FRAME_HEIGHT);
    cv::VideoWriter video("outcpp.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
    cv::Mat frame; //will contain the frame

    //for (auto i = 0; i < num_frames; i++)
    {
        camera >> frame; //get the frame
        //cv::cvtColor(frame, frame, BGR2RGB);
        for (auto i = 0; i < frame_width * frame_height; i++)
        {
            frame.data[3 * i + 1] = 0;
            //frame.data[3 * i + 2] = 0;
            frame.data[3 * i] = 0;
        }

        video.write(frame); //write the frames
    }
    printf("Escribiendo frame a 'outcpp.avi'\n");
    video.release();
    camera.release();
    printf("Done\n");
    return 0;
}
