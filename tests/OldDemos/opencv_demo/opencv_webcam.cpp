#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/videoio.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <iostream>
#include "bmp.h"
#include "gauss.h"
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
int main(int argc, char **argv)
{

    if (argc < 4)
    {
        std::cerr << "USAGE ./webcam <num_webcam> <num_seconds> <FPS>" << std::endl;
    }

    // open the first webcam plugged in the computer
    cv::VideoCapture camera(atoi(argv[1]));
    int t_sec = atoi(argv[2]);
    int fps = atoi(argv[3]);
    int num_frames = t_sec * fps;
    printf("Generating video with t=%d sec, FPS=%d, Number of frames = %d\n", t_sec, fps, num_frames);
    int filterWidth = 9;

    float *filter = createFilter(filterWidth);

    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }

    // Default resolution of the frame is obtained.The default resolution is system dependent.
    int frame_width = camera.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = camera.get(cv::CAP_PROP_FRAME_HEIGHT);

    cv::VideoWriter video("outcpp_blurred.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
    cv::Mat frame[num_frames]; //will contain the frame

    pixel_bgr *blurred_frame[num_frames];

    printf("Compute starting\n");
    for (auto i = 0; i < num_frames; i++)
    {
        camera >> frame[i]; //get the frame
        usleep(1000000.0 / fps);
    }

    for (auto i = 0; i < num_frames; i++)
    {
        blurred_frame[i] = (pixel_bgr *)malloc(frame_width * frame_height * sizeof(char) * 3);
        GaussianBlurOnCPU(blurred_frame[i], (pixel_bgr *)frame[i].data, frame_height, frame_width, filter, filterWidth);
        frame[i].data = (uchar *)blurred_frame[i];
    }

    printf("Writing to'outcpp.avi'\n");
    for (auto i = 0; i < num_frames; i++)
    {
        video.write(frame[i]);
    }

    printf("freeing resources\n");
    for (auto i = 0; i < num_frames; i++)
    {
        free(blurred_frame[i]);
    }
    camera.release();
    video.release();
    printf("Done\n");
    return 0;
}
