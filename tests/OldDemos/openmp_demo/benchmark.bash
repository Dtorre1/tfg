#!/bin/bash
declare -a sizes=("640 480" "320 240" "160 120")
cd 4I4T
make
for func in 0 1;
do
	for size in "${sizes[@]}";
	do
		for buffer in 1 5 10 20 40 100;
		do
			for j in {1..12};
			do
				./omp_cv_webcam /dev/video0 10 20 $size $func $buffer >> pi_v4l2.txt
				sleep 5
			done
		done
	done
done
#cd ..
#cd 1I4T
#make
#for size in "${sizes[@]}";
#do
#	for j in {1..12};
#	do
#		for func in 0 1;
#		do
#			./omp_cv_webcam 0 10 20 $size $func 20 >> pi.txt
#			sleep 2
#		done
#	done
#done

