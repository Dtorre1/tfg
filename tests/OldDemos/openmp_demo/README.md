Two different versions of this demo on OpenMp:
4T4I - 4 threads, each one of them computes a differente image or frame
4T1I - 4 threads, each one of them computes a chunk from the same image

Notas:
Hace falta hacer benchmarking para obtener datos utilizables
pero a ojo parece que no hay diferencia notable entre hacer 4 images o 1 paralelamente.

Tarda aprox 12s en computar un video de 20 frames por lo que no parece que sirva para tiempo real.

Tocará GPU probablemente.
