#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <sys/time.h>

#include "bmp.h"
#include "gauss.h"
#include "webcam_capture.hpp"

int main(int argc, char **argv)
{
    struct timeval timer_start, timer_end, frame_start, frame_end;
    double compute_time, ini_time;
    double frame_time = 0.0;
    gettimeofday(&timer_start, NULL);

    if (argc < 8)
    {
        std::cerr << "USAGE ./webcam </dev/videoN> <num_seconds> <FPS> <width> <height> <filter> <buffer_size>" << std::endl;
        std::cerr << "For filter use 0 for gauss and 1 for grayscale" << std::endl;
        std::cerr << "Buffer_size is measured in frames ex: 5 frame buffer" << std::endl;
        exit(1);
    }

    // open the first webcam plugged in the computer
    char *devname = argv[1];
    int t_sec = atoi(argv[2]);
    int fps = atoi(argv[3]);
    int num_frames = t_sec * fps;
    int frame_width = atoi(argv[4]);
    int frame_height = atoi(argv[5]);
    int filterWidth = 9;
    float *filter = createFilter(filterWidth);
    int function = atoi(argv[6]);
    cameraInit(devname, frame_width, frame_height);
    if (function != 0 && function != 1)
    {
        std::cerr << "For filter use 0 for gauss and 1 for grayscale" << std::endl;
        v4l2_close();
        exit(1);
    }
    int buffer_size = atoi(argv[7]);
    if (buffer_size <= 0 || num_frames % buffer_size != 0)
    {
        std::cerr << "Buffer cannot be lower than 1 and must be a divisor of num_frames" << std::endl;
        v4l2_close();
        exit(1);
    }

    cv::VideoWriter video("outcpp_blurred.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
    int imgSize = frame_height * frame_width;
    char **frame_buffer = (char **)malloc(buffer_size * sizeof(char *)); //will contain the frames
    char **frame_auxiliar_buffer = (char **)malloc(buffer_size * sizeof(char *));
    pixel_bgr **store_buffer = (pixel_bgr **)malloc(buffer_size * sizeof(pixel_bgr *));
    pixel_bgr **blurred_frame = (pixel_bgr **)malloc(buffer_size * sizeof(pixel_bgr *));
    for(auto i = 0 ; i < buffer_size ; i++)
    {
        frame_buffer[i] = (char *) malloc(imgSize * sizeof(pixel_bgr));
        frame_auxiliar_buffer[i] = (char *) malloc(imgSize * sizeof(pixel_bgr));
        store_buffer[i] = (pixel_bgr *) malloc(imgSize * sizeof(pixel_bgr));
        blurred_frame[i] = (pixel_bgr *) malloc(imgSize * sizeof(pixel_bgr));
    }
    gettimeofday(&timer_end, NULL);
    ini_time = timer_difference(timer_end, timer_start);

    gettimeofday(&timer_start, NULL);
    int num_buffers = num_frames / buffer_size;

    #pragma omp parallel num_threads(4)
    {
        #pragma omp single
        {
            for (auto j = 0; j < num_buffers; j++)
            {
                for (auto i = 0; i < buffer_size; i++)
                {
                    gettimeofday(&frame_start, NULL);
                    grabFrame(frame_buffer[i]); //get the frame
                    gettimeofday(&frame_end, NULL);
                    frame_time += timer_difference(frame_end, frame_start);
                    #pragma omp task
                    {
                        if (function == 0)
                        {
                            GaussianBlurOnCPU(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width, filter, filterWidth);
                        }
                        else
                        {
                            GrayscaleOnCPU(blurred_frame[i], (pixel_bgr *)frame_buffer[i], frame_height, frame_width);
                        }
                    }
                }
                #pragma omp taskwait
                
                pixel_bgr **buffer_aux;
                char **aux;

                aux = frame_buffer;
                frame_buffer = frame_auxiliar_buffer;
                frame_auxiliar_buffer = aux;

                buffer_aux = blurred_frame;
                blurred_frame = store_buffer;
                store_buffer = buffer_aux;
                #pragma omp task
                {
                    for (auto i = 0; i < buffer_size; i++)
                    {
                        cv::Mat cvFrame(frame_height, frame_width, CV_8UC3, blurred_frame[i]);
                        std::memcpy(cvFrame.data, blurred_frame[i], imgSize * sizeof(pixel_bgr));
                        video.write(cvFrame);
                    }
                }
                
            }
        }
        #pragma omp taskwait
    }
    
    gettimeofday(&timer_end, NULL);
    compute_time = timer_difference(timer_end, timer_start);

    video.release();
    v4l2_close();

    for(auto i = 0; i < buffer_size; i++)
    {
        free(frame_auxiliar_buffer[i]);
        free(frame_buffer[i]);
        free(blurred_frame[i]);
        free(store_buffer[i]);
    }
    free(frame_auxiliar_buffer);
    free(frame_buffer);
    free(blurred_frame);
    free(store_buffer);

    char *func;
    if (function == 0)
    {
        func = "gauss\0";
    }
    else
    {
        func = "grayscale\0";
    }
    printf("%d:%d:%d:task:buffer-%d:%f:record+%s:%f:%f\n", 
        frame_width, frame_height, num_frames, buffer_size, compute_time, func, ini_time, frame_time/num_frames);
    return 0;
}
