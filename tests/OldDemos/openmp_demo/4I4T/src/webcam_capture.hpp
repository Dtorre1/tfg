#ifndef WEBCAM_CAPTURE_H
#define WEBCAM_CAPTURE_H
#include "bmp.h"
/**
 * Functions
 * */
static void xioctl(int fh, int request, void *arg);
void storeImageHelper(char *filename, char *rgbData, int width, int height);
int cameraInit(char *dev_name, int width, int height);
int grabFrame(char *dest);
void v4l2_close();
double timer_difference(struct timeval t2, struct timeval t1);

#endif