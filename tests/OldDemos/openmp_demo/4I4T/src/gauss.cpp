#include "bmp.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>

void GaussianBlurOnCPU(pixel_bgr *dst, pixel_bgr *src, int rows, int cols, float *filter, int filterWidth)
{
    int row;
    int col;
    int i, j;

    const int half = filterWidth / 2;
    const int width = cols - 1;
    const int height = rows - 1;
    for (row = 0; row < rows; row++)
    {
        for (col = 0; col < cols; col++)
        {
            float blur_red = 0;
            float blur_green = 0;
            float blur_blue = 0;
            for (i = -half; i <= half; ++i)
            {
                int h = std::min(std::max(row + i, 0), height);
                for (j = -half; j <= half; ++j)
                {
                    // Clamp filter to the image border

                    int w = std::min(std::max(col + j, 0), width);
                    int idx = w + cols * h;                        // current pixel index
                    int idf = (i + half) * filterWidth + j + half; //current filter index
                    float red = (float)(src[idx].rojo);
                    float green = (float)(src[idx].verde);
                    float blue = (float)(src[idx].azul);
                    float weight = filter[idf];
                    blur_red += red * weight;
                    blur_green += green * weight;
                    blur_blue += blue * weight;
                }
            }
            dst[row * cols + col].rojo = (unsigned char)(blur_red);
            dst[row * cols + col].verde = (unsigned char)(blur_green);
            dst[row * cols + col].azul = (unsigned char)(blur_blue);
        }
    }
}

float *createFilter(int width)
{
    const float sigma = 2.f; // Standard deviation of the Gaussian distribution.

    const int half = width / 2;
    float sum = 0.f;

    // Create convolution matrix
    float *res = (float *)malloc(width * width * sizeof(float));

    // Calculate filter sum first
    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            // e (natural logarithm base) to the power x, where x is what's in the brackets
            float weight = expf((float)(c * c + r * r) / (2.f * sigma * sigma));
            int idx = (r + half) * width + c + half;

            res[idx] = weight;
            sum += weight;
        }
    }

    // Normalize weight: sum of weights must equal 1
    float normal = 1.f / sum;

    for (int r = -half; r <= half; ++r)
    {
        for (int c = -half; c <= half; ++c)
        {
            int idx = (r + half) * width + c + half;

            res[idx] *= normal;
        }
    }
    return res;
}
//based on https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm
void GrayscaleOnCPU(pixel_bgr *dst, pixel_bgr *src, int rows, int cols)
{
    int i, j, index;
    unsigned char col;
    pixel_bgr pix;
    for (i = 0; i < rows; i++)
    {
        for (j = 0; j < cols; j++)
        {
            index = i * cols + j;
            pix = src[index];
            col = (unsigned char)(pix.rojo * 0.3 + pix.verde * 0.59 + pix.azul * 0.11);
            dst[index].rojo = col;
            dst[index].verde = col;
            dst[index].azul = col;
        }
    }
}