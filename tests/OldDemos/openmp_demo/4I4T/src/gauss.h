#ifndef GAUSS_H
#define GAUSS_H
void GaussianBlurOnCPU(pixel_bgr *dst, pixel_bgr *src, int rows, int cols, float *filter, int filterWidth);
void GrayscaleOnCPU(pixel_bgr *dst, pixel_bgr *src, int rows, int cols);
float *createFilter(int width);
#endif