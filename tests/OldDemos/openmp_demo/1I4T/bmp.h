//include guard start
#ifndef BMP_H
#define BMP_H

#include <stdint.h>


typedef struct pixel_bgr
{
  unsigned char azul;
  unsigned char verde;
  unsigned char rojo;
} pixel_bgr;


#endif
//Include guard end