#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "bmp.h"
#include "gauss.h"

double timer_difference(struct timeval t2, struct timeval t1) //t2 - t1
{
    return ((double)(t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec) / 1000000.0;
}

int main(int argc, char **argv)
{
    struct timeval timer_start, timer_end;
    double compute_time, ini_time;
    gettimeofday(&timer_start, NULL);

    if (argc < 8)
    {
        std::cerr << "USAGE ./webcam <num_webcam> <num_seconds> <FPS> <width> <height> <filter> <buffer_size>" << std::endl;
        std::cerr << "For filter use 0 for gauss and 1 for grayscale" << std::endl;
        std::cerr << "Buffer_size is measured in frames ex: 5 frame buffer" << std::endl;
        exit(1);
    }

    // open the first webcam plugged in the computer
    cv::VideoCapture camera(atoi(argv[1]));
    int t_sec = atoi(argv[2]);
    int fps = atoi(argv[3]);
    int num_frames = t_sec * fps;
    int filterWidth = 9;

    float *filter = createFilter(filterWidth);

    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }

    int frame_width = atoi(argv[4]);
    int frame_height = atoi(argv[5]);
    camera.set(cv::CAP_PROP_FRAME_WIDTH, frame_width);
    camera.set(cv::CAP_PROP_FRAME_HEIGHT, frame_height);
    int function = atoi(argv[6]);
    if (function != 0 && function != 1)
    {
        std::cerr << "For filter use 0 for gauss and 1 for grayscale" << std::endl;
        camera.release();
        exit(1);
    }
    int buffer_size = atoi(argv[7]);
    if (buffer_size <= 0 || fps % buffer_size != 0)
    {
        std::cerr << "Buffer cannot be lower than 1 and must be a divisor of FPS" << std::endl;
        camera.release();
        exit(1);
    }

    cv::VideoWriter video("outcpp_blurred.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, cv::Size(frame_width, frame_height));
    cv::Mat frame[num_frames]; //will contain the frame

    pixel_bgr *blurred_frame[num_frames];
    gettimeofday(&timer_end, NULL);
    ini_time = timer_difference(timer_end, timer_start);

    gettimeofday(&timer_start, NULL);
    int num_buffers = num_frames / buffer_size;
    for(auto i = 0; i < buffer_size; i++)
    {
        blurred_frame[i] = (pixel_bgr *)malloc(frame_width * frame_height * sizeof(pixel_bgr));
    }
    for (auto j = 0; j < num_buffers; j++)
    {
        for (auto i = 0; i < buffer_size; i++)
        {
            camera >> frame[i]; //get the frame
            if (function == 0)
            {
                GaussianBlurOnCPU(blurred_frame[i], (pixel_bgr *)frame[i].data, frame_height, frame_width, filter, filterWidth);
            }
            else
            {
                GrayscaleOnCPU(blurred_frame[i], (pixel_bgr *)frame[i].data, frame_height, frame_width);
            }
            frame[i].data = (uchar *)blurred_frame[i];
        }
        for (auto i = 0; i < buffer_size; i++)
        {
            video.write(frame[i]);
        }
    }
    gettimeofday(&timer_end, NULL);

    compute_time = timer_difference(timer_end, timer_start);

    for (auto i = 0; i < num_frames; i++)
    {
        video.write(frame[i]);
    }

    for (auto i = 0; i < num_frames; i++)
    {
        free(blurred_frame[i]);
    }
    camera.release();
    video.release();
    char *func;
    if (function == 0)
    {
        func = "gauss\0";
    }
    else
    {
        func = "grayscale\0";
    }

    printf("%d:%d:%d:data-paralellism:buffer-%d:%f:record+%s:%f\n", frame_width, frame_height, num_frames, buffer_size, compute_time, func, ini_time);
    return 0;
}
